﻿using Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Core
{
    public class TimesheetGridValidationModel
    {
        public int EmployeeId { get; set; }
        public bool Exempt { get; set; }
        public string ValidationSummary { get; set; }

        public decimal KronosSundayRegularTotal { get; set; }
        public decimal KronosMondayRegularTotal { get; set; }
        public decimal KronosTuesdayRegularTotal { get; set; }
        public decimal KronosWednesdayRegularTotal { get; set; }
        public decimal KronosThursdayRegularTotal { get; set; }
        public decimal KronosFridayRegularTotal { get; set; }
        public decimal KronosSaturdayRegularTotal { get; set; }

        public decimal KronosSundayOvertimeTotal { get; set; }
        public decimal KronosMondayOvertimeTotal { get; set; }
        public decimal KronosTuesdayOvertimeTotal { get; set; }
        public decimal KronosWednesdayOvertimeTotal { get; set; }
        public decimal KronosThursdayOvertimeTotal { get; set; }
        public decimal KronosFridayOvertimeTotal { get; set; }
        public decimal KronosSaturdayOvertimeTotal { get; set; }

        public decimal DistributionSundayRegularTotal { get; set; }
        public decimal DistributionMondayRegularTotal { get; set; }
        public decimal DistributionTuesdayRegularTotal { get; set; }
        public decimal DistributionWednesdayRegularTotal { get; set; }
        public decimal DistributionThursdayRegularTotal { get; set; }
        public decimal DistributionFridayRegularTotal { get; set; }
        public decimal DistributionSaturdayRegularTotal { get; set; }

        public decimal DistributionSundayOvertimeTotal { get; set; }
        public decimal DistributionMondayOvertimeTotal { get; set; }
        public decimal DistributionTuesdayOvertimeTotal { get; set; }
        public decimal DistributionWednesdayOvertimeTotal { get; set; }
        public decimal DistributionThursdayOvertimeTotal { get; set; }
        public decimal DistributionFridayOvertimeTotal { get; set; }
        public decimal DistributionSaturdayOvertimeTotal { get; set; }

        public List<WfcTotalModel> WfcTotalList { get; set; }
        public List<TimesheetDistribution> DistributionTotalList { get; set; }
        public List<int> SupervisorIdList { get; set; }
        public List<ErrorInfo> ErrorList { get; set; }

        public TimesheetPeriod TimesheetPeriod { get; set; }

        public void SetTotalProperties()
        {
            KronosSundayRegularTotal = decimal.Round(WfcTotalList.Where(w => w.TimesheetDate.DayOfWeek == DayOfWeek.Sunday && (w.Regular || w.Display)).Sum(w => w.TimesheeTotal), 2);
            KronosMondayRegularTotal = decimal.Round(WfcTotalList.Where(w => w.TimesheetDate.DayOfWeek == DayOfWeek.Monday && (w.Regular || w.Display)).Sum(w => w.TimesheeTotal), 2);
            KronosTuesdayRegularTotal = decimal.Round(WfcTotalList.Where(w => w.TimesheetDate.DayOfWeek == DayOfWeek.Tuesday && (w.Regular || w.Display)).Sum(w => w.TimesheeTotal), 2);
            KronosWednesdayRegularTotal = decimal.Round(WfcTotalList.Where(w => w.TimesheetDate.DayOfWeek == DayOfWeek.Wednesday && (w.Regular || w.Display)).Sum(w => w.TimesheeTotal), 2);
            KronosThursdayRegularTotal = decimal.Round(WfcTotalList.Where(w => w.TimesheetDate.DayOfWeek == DayOfWeek.Thursday && (w.Regular || w.Display)).Sum(w => w.TimesheeTotal), 2);
            KronosFridayRegularTotal = decimal.Round(WfcTotalList.Where(w => w.TimesheetDate.DayOfWeek == DayOfWeek.Friday && (w.Regular || w.Display)).Sum(w => w.TimesheeTotal), 2);
            KronosSaturdayRegularTotal = decimal.Round(WfcTotalList.Where(w => w.TimesheetDate.DayOfWeek == DayOfWeek.Saturday && (w.Regular || w.Display)).Sum(w => w.TimesheeTotal), 2);

            KronosSundayOvertimeTotal = decimal.Round(WfcTotalList.Where(w => w.TimesheetDate.DayOfWeek == DayOfWeek.Sunday && (w.Overtime)).Sum(w => w.TimesheeTotal), 2);
            KronosMondayOvertimeTotal = decimal.Round(WfcTotalList.Where(w => w.TimesheetDate.DayOfWeek == DayOfWeek.Monday && (w.Overtime)).Sum(w => w.TimesheeTotal), 2);
            KronosTuesdayOvertimeTotal = decimal.Round(WfcTotalList.Where(w => w.TimesheetDate.DayOfWeek == DayOfWeek.Tuesday && (w.Overtime)).Sum(w => w.TimesheeTotal), 2);
            KronosWednesdayOvertimeTotal = decimal.Round(WfcTotalList.Where(w => w.TimesheetDate.DayOfWeek == DayOfWeek.Wednesday && (w.Overtime)).Sum(w => w.TimesheeTotal), 2);
            KronosThursdayOvertimeTotal = decimal.Round(WfcTotalList.Where(w => w.TimesheetDate.DayOfWeek == DayOfWeek.Thursday && (w.Overtime)).Sum(w => w.TimesheeTotal), 2);
            KronosFridayOvertimeTotal = decimal.Round(WfcTotalList.Where(w => w.TimesheetDate.DayOfWeek == DayOfWeek.Friday && (w.Overtime)).Sum(w => w.TimesheeTotal), 2);
            KronosSaturdayOvertimeTotal = decimal.Round(WfcTotalList.Where(w => w.TimesheetDate.DayOfWeek == DayOfWeek.Saturday && (w.Overtime)).Sum(w => w.TimesheeTotal), 2);

            DistributionSundayRegularTotal = DistributionTotalList.Sum(w => w.SundayRegular);
            DistributionMondayRegularTotal = DistributionTotalList.Sum(w => w.MondayRegular);
            DistributionTuesdayRegularTotal = DistributionTotalList.Sum(w => w.TuesdayRegular);
            DistributionWednesdayRegularTotal = DistributionTotalList.Sum(w => w.WednesdayRegular);
            DistributionThursdayRegularTotal = DistributionTotalList.Sum(w => w.ThursdayRegular);
            DistributionFridayRegularTotal = DistributionTotalList.Sum(w => w.FridayRegular);
            DistributionSaturdayRegularTotal = DistributionTotalList.Sum(w => w.SaturdayRegular);

            DistributionSundayOvertimeTotal = DistributionTotalList.Sum(w => w.SundayOvertime);
            DistributionMondayOvertimeTotal = DistributionTotalList.Sum(w => w.MondayOvertime);
            DistributionTuesdayOvertimeTotal = DistributionTotalList.Sum(w => w.TuesdayOvertime);
            DistributionWednesdayOvertimeTotal = DistributionTotalList.Sum(w => w.WednesdayOvertime);
            DistributionThursdayOvertimeTotal = DistributionTotalList.Sum(w => w.ThursdayOvertime);
            DistributionFridayOvertimeTotal = DistributionTotalList.Sum(w => w.FridayOvertime);
            DistributionSaturdayOvertimeTotal = DistributionTotalList.Sum(w => w.SaturdayOvertime);
        }

        public TimesheetGridValidationModel(int employeeId, DateTime timesheetDate, TimesheetGridModel item)
        {
            ///Initialize Collections
            WfcTotalList = new List<WfcTotalModel>();
            DistributionTotalList = new List<TimesheetDistribution>();
            SupervisorIdList = new List<int>();
            ErrorList = new List<ErrorInfo>();

            EmployeeId = employeeId;
            ///Get Exempt Value from employeee
            var kronosContext = new KronosEntities();
            bool exempt = false;

            PERSONCSTMDATA customData = kronosContext.PERSONCSTMDATAs.Where(c => c.PERSONID == employeeId && c.CUSTOMDATADEFID == 7).FirstOrDefault();
            if (customData != null && customData.PERSONCSTMDATATXT.ToLower() == "exempt")
            {
                exempt = true;
            }

            Exempt = exempt;

            ///Get Distribution Info
            DateTime fromDate, toDate;
            Helper.GetPeriodDates(timesheetDate, out fromDate, out toDate);

            TimesheetPeriod period = new SSSEntities().TimesheetPeriods.Where(d => d.EmployeeId == employeeId && d.FromDate <= timesheetDate && d.ToDate >= timesheetDate).FirstOrDefault();
            if (period == null)
            {
                period = new TimesheetPeriod { };
            }

            TimesheetPeriod = period;

            int updatingDistributionId = 0;
            if (item != null)
            {
                updatingDistributionId = item.Id;
            }

            DistributionTotalList = period.TimesheetDistributions.Where(d => d.Id != updatingDistributionId).ToList();

            ///Get WfCTotals
            WfcTotalModel wfcTotalModel = new WfcTotalModel();
            WfcTotalList = wfcTotalModel.GetWfcTotal(employeeId, fromDate, toDate);

            SetTotalProperties();


        }

        public void ValidatePeriod(string action, int loggedUserId)
        {
            switch (action)
            {
                case "submit":
                    {
                        if (TimesheetPeriod.Status != null && TimesheetPeriod.Status.ToLower() != "pendingsubmission" && TimesheetPeriod.Status.ToLower() != "revision")
                        {
                            ValidationSummary += "<div class='col-md-12'>" + Resource.OnlyPendingToSubmitOrRevision + "</div>";
                        }

                        if (loggedUserId != TimesheetPeriod.EmployeeId)
                        {
                            ValidationSummary += "<div class='col-md-12'>" + Resource.OnlyEmployeesCanSubmitTheirTimesheets + "</div>";
                        }

                        break;
                    }

                case "approve":
                    {
                        if (TimesheetPeriod.Status != null && TimesheetPeriod.Status.ToLower() != "submitted")
                        {
                            ValidationSummary += "<div class='col-md-12'>" + Resource.OnlySubmittedCanBeApproved + "</div>";
                        }

                        List<int> employeesForSypervisor = new EmployeeInfoModel().GetEmployeeIdsForSupervisor(loggedUserId);
                        if (!employeesForSypervisor.Contains(TimesheetPeriod.EmployeeId))
                        {
                            ValidationSummary += "<div class='col-md-12'>" + Resource.OnlySupervisorOrDelegateCanApproveTimesheets + "</div>";
                        }

                        break;
                    }

                case "revision":
                    {
                        if (TimesheetPeriod.Status != null && TimesheetPeriod.Status.ToLower() != "submitted")
                        {
                            ValidationSummary += "<div class='col-md-12'>" + Resource.OnlySubmittedCanBeSendToRevision + "</div>";
                        }

                        List<int> employeesForSypervisor = new EmployeeInfoModel().GetEmployeeIdsForSupervisor(loggedUserId);
                        if (!employeesForSypervisor.Contains(TimesheetPeriod.EmployeeId))
                        {
                            ValidationSummary += "<div class='col-md-12'>" + Resource.OnlySupervisorOrDelegateCanSendToRevisionTimesheets + "</div>";
                        }

                        break;
                    }
            }
            EmployeeInfoModel empInfoModel = new EmployeeInfoModel();
            List<SupervisorInfoModel> supList = empInfoModel.GetSupervisorList(EmployeeId);
            foreach (SupervisorInfoModel item in supList)
            {
                SupervisorIdList.Add(item.PersonId);
            }

            if (SupervisorIdList.Count == 0)
            {
                ValidationSummary += "<div class='col-md-12'>" + Resource.NoSupervisorDefined + "</div>";
            }

            if (
            (
              KronosSundayRegularTotal + KronosSundayOvertimeTotal
            + KronosMondayRegularTotal + KronosMondayOvertimeTotal
            + KronosTuesdayRegularTotal + KronosTuesdayOvertimeTotal
            + KronosWednesdayRegularTotal + KronosWednesdayOvertimeTotal
            + KronosThursdayRegularTotal + KronosThursdayOvertimeTotal
            + KronosFridayRegularTotal + KronosFridayOvertimeTotal
            + KronosSaturdayRegularTotal + KronosSaturdayOvertimeTotal
            ) == 0
           )
            {
                ValidationSummary += "<div class='col-md-12'>" + Resource.NoKronosHoursToAllocate + "</div>";
            }

            if (Exempt)
            {
                if (DistributionSundayOvertimeTotal > 0 ||
                    DistributionMondayOvertimeTotal > 0 ||
                    DistributionTuesdayOvertimeTotal > 0 ||
                    DistributionWednesdayOvertimeTotal > 0 ||
                    DistributionThursdayOvertimeTotal > 0 ||
                    DistributionFridayOvertimeTotal > 0 ||
                    DistributionSaturdayOvertimeTotal > 0)
                {
                    ValidationSummary += "<div class='col-md-12'>" + Resource.ExemptEmployeesMustNotReportOvertime + "</div>";
                }

                if (DistributionSundayRegularTotal < KronosSundayRegularTotal)
                {
                    ValidationSummary += "<div class='col-md-12'>" + Resource.AllocatedHoursAreLessThanHoursInKronos + " (" + Resource.Sunday + ")</div>";
                }

                if (DistributionMondayRegularTotal < KronosMondayRegularTotal)
                {
                    ValidationSummary += "<div class='col-md-12'>" + Resource.AllocatedHoursAreLessThanHoursInKronos + " (" + Resource.Monday + ")</div>";
                }

                if (DistributionTuesdayRegularTotal < KronosTuesdayRegularTotal)
                {
                    ValidationSummary += "<div class='col-md-12'>" + Resource.AllocatedHoursAreLessThanHoursInKronos + " (" + Resource.Tuesday + ")</div>";
                }

                if (DistributionWednesdayRegularTotal < KronosWednesdayRegularTotal)
                {
                    ValidationSummary += "<div class='col-md-12'>" + Resource.AllocatedHoursAreLessThanHoursInKronos + " (" + Resource.Wednesday + ")</div>";
                }

                if (DistributionThursdayRegularTotal < KronosThursdayRegularTotal)
                {
                    ValidationSummary += "<div class='col-md-12'>" + Resource.AllocatedHoursAreLessThanHoursInKronos + " (" + Resource.Thursday + ")</div>";
                }

                if (DistributionFridayRegularTotal < KronosFridayRegularTotal)
                {
                    ValidationSummary += "<div class='col-md-12'>" + Resource.AllocatedHoursAreLessThanHoursInKronos + " (" + Resource.Friday + ")</div>";
                }

                if (DistributionSaturdayRegularTotal < KronosSaturdayRegularTotal)
                {
                    ValidationSummary += "<div class='col-md-12'>" + Resource.AllocatedHoursAreLessThanHoursInKronos + " (" + Resource.Saturday + ")</div>";
                }

            }
            else
            {
                if ((DistributionSundayRegularTotal + DistributionSundayOvertimeTotal) != (KronosSundayRegularTotal + KronosSundayOvertimeTotal))
                {
                    ValidationSummary += "<div class='col-md-12'>" + Resource.AllocatedHoursMustBeEqualToKronosHours + " (" + Resource.Sunday + ")</div>";
                }

                if ((DistributionMondayRegularTotal + DistributionMondayOvertimeTotal) != (KronosMondayRegularTotal + KronosMondayOvertimeTotal))
                {
                    ValidationSummary += "<div class='col-md-12'>" + Resource.AllocatedHoursMustBeEqualToKronosHours + " (" + Resource.Monday + ")</div>";
                }

                if ((DistributionTuesdayRegularTotal + DistributionTuesdayOvertimeTotal) != (KronosTuesdayRegularTotal + KronosTuesdayOvertimeTotal))
                {
                    ValidationSummary += "<div class='col-md-12'>" + Resource.AllocatedHoursMustBeEqualToKronosHours + " (" + Resource.Tuesday + ")</div>";
                }

                if ((DistributionWednesdayRegularTotal + DistributionWednesdayOvertimeTotal) != (KronosWednesdayRegularTotal + KronosWednesdayOvertimeTotal))
                {
                    ValidationSummary += "<div class='col-md-12'>" + Resource.AllocatedHoursMustBeEqualToKronosHours + " (" + Resource.Wednesday + ")</div>";
                }

                if ((DistributionThursdayRegularTotal + DistributionThursdayOvertimeTotal) != (KronosThursdayRegularTotal + KronosThursdayOvertimeTotal))
                {
                    ValidationSummary += "<div class='col-md-12'>" + Resource.AllocatedHoursMustBeEqualToKronosHours + " (" + Resource.Thursday + ")</div>";
                }

                if ((DistributionFridayRegularTotal + DistributionFridayOvertimeTotal) != (KronosFridayRegularTotal + KronosFridayOvertimeTotal))
                {
                    ValidationSummary += "<div class='col-md-12'>" + Resource.AllocatedHoursMustBeEqualToKronosHours + " (" + Resource.Friday + ")</div>";
                }

                if ((DistributionSaturdayRegularTotal + DistributionSaturdayOvertimeTotal) != (KronosSaturdayRegularTotal + KronosSaturdayOvertimeTotal))
                {
                    ValidationSummary += "<div class='col-md-12'>" + Resource.AllocatedHoursMustBeEqualToKronosHours + " (" + Resource.Saturday + ")</div>";
                }
            }

        }

        public void ValidateEntry(TimesheetGridModel item, int loggedUserId)
        {
            List<int> employeesForSypervisor = new EmployeeInfoModel().GetEmployeeIdsForSupervisor(loggedUserId);
            if (loggedUserId != EmployeeId && !employeesForSypervisor.Contains(EmployeeId))
            {
                ErrorList.Add(new ErrorInfo { FieldName = "ConceptId", ErrorMessage = Resource.UserDontHavePermissionToThisAction });
            }

            if (TimesheetPeriod.Status != null && TimesheetPeriod.Status.ToLower() != "pendingsubmission" && TimesheetPeriod.Status.ToLower() != "revision" && TimesheetPeriod.Status.ToLower() != "submitted")
            {
                ErrorList.Add(new ErrorInfo { FieldName = "ConceptId", ErrorMessage = Resource.OnlyPendingToSubmitRevisionAndSubmitedCanBeEdited });
            }
            else
            {
                if ((TimesheetPeriod.Status != null && TimesheetPeriod.Status.ToLower() == "pendingsubmission" || TimesheetPeriod.Status.ToLower() == "revision") && EmployeeId !=loggedUserId)
                {
                    ErrorList.Add(new ErrorInfo { FieldName = "ConceptId", ErrorMessage = Resource.OnlyEmployeeCanEditPendingToSubmitOrRevisionTimesheet });
                }

                if (TimesheetPeriod.Status != null && TimesheetPeriod.Status.ToLower() == "submitted" && !employeesForSypervisor.Contains(EmployeeId))
                {
                    ErrorList.Add(new ErrorInfo { FieldName = "ConceptId", ErrorMessage = Resource.OnlySupervisorOrDelegateCanEditSubmittedTimesheet });
                }
            }

            if (item.ConceptId == 0)
            {
                ErrorList.Add(new ErrorInfo { FieldName = "ConceptId", ErrorMessage = Resource.Concept + " " + Resource.IsRequired, });
            }

            if (item.CostCenterId == null || item.CostCenterId == 0)
            {
                ErrorList.Add(new ErrorInfo { FieldName = "CostCenterId", ErrorMessage = Resource.CostCenter + " " + Resource.IsRequired, });
            }

            if (
                    (item.SundayReg + item.SundayOv
                    + item.MondayReg + item.MondayOv
                    + item.TuesdayReg + item.TuesdayOv
                    + item.WednesdayReg + item.WednesdayOv
                    + item.ThursdayReg + item.ThursdayOv
                    + item.FridayReg + item.FridayOv
                    + item.SaturdayReg + item.SaturdayOv) == 0
                )
            {
                ErrorList.Add(new ErrorInfo { FieldName = "MondayReg", ErrorMessage = Resource.PleaseAllocateAtLeastOneDay, });
            }

            if (
                (
                  KronosSundayRegularTotal + KronosSundayOvertimeTotal
                + KronosMondayRegularTotal + KronosMondayOvertimeTotal
                + KronosTuesdayRegularTotal + KronosTuesdayOvertimeTotal
                + KronosWednesdayRegularTotal + KronosWednesdayOvertimeTotal
                + KronosThursdayRegularTotal + KronosThursdayOvertimeTotal
                + KronosFridayRegularTotal + KronosFridayOvertimeTotal
                + KronosSaturdayRegularTotal + KronosSaturdayOvertimeTotal
                ) == 0
               )
            {
                ErrorList.Add(new ErrorInfo { FieldName = "MondayReg", ErrorMessage = Resource.NoKronosHoursToAllocate, });
            }


            if (Exempt)
            {
                if (item.SundayOv > 0)
                {
                    ErrorList.Add(new ErrorInfo { FieldName = "SundayOv", ErrorMessage = Resource.ExemptEmployeesMustNotReportOvertime, });
                }

                if (item.MondayOv > 0)
                {
                    ErrorList.Add(new ErrorInfo { FieldName = "MondayOv", ErrorMessage = Resource.ExemptEmployeesMustNotReportOvertime, });
                }

                if (item.TuesdayOv > 0)
                {
                    ErrorList.Add(new ErrorInfo { FieldName = "TuesdayOv", ErrorMessage = Resource.ExemptEmployeesMustNotReportOvertime, });
                }

                if (item.WednesdayOv > 0)
                {
                    ErrorList.Add(new ErrorInfo { FieldName = "WednesdayOv", ErrorMessage = Resource.ExemptEmployeesMustNotReportOvertime, });
                }

                if (item.ThursdayOv > 0)
                {
                    ErrorList.Add(new ErrorInfo { FieldName = "ThursdayOv", ErrorMessage = Resource.ExemptEmployeesMustNotReportOvertime, });
                }

                if (item.FridayOv > 0)
                {
                    ErrorList.Add(new ErrorInfo { FieldName = "FridayOv", ErrorMessage = Resource.ExemptEmployeesMustNotReportOvertime, });
                }

                if (item.SaturdayOv > 0)
                {
                    ErrorList.Add(new ErrorInfo { FieldName = "SaturdayOv", ErrorMessage = Resource.ExemptEmployeesMustNotReportOvertime, });
                }
            }
            else
            {
                //Regular hours not exempt validation
                if ((DistributionSundayRegularTotal + item.SundayReg) > KronosSundayRegularTotal)
                {
                    ErrorList.Add(new ErrorInfo { FieldName = "SundayReg", ErrorMessage = Resource.AllocationMustNotExceedKronosHours, });
                }

                if ((DistributionMondayRegularTotal + item.MondayReg) > KronosMondayRegularTotal)
                {
                    ErrorList.Add(new ErrorInfo { FieldName = "MondayReg", ErrorMessage = Resource.AllocationMustNotExceedKronosHours, });
                }

                if ((DistributionTuesdayRegularTotal + item.TuesdayReg) > KronosTuesdayRegularTotal)
                {
                    ErrorList.Add(new ErrorInfo { FieldName = "TuesdayReg", ErrorMessage = Resource.AllocationMustNotExceedKronosHours, });
                }

                if ((DistributionWednesdayRegularTotal + item.WednesdayReg) > KronosWednesdayRegularTotal)
                {
                    ErrorList.Add(new ErrorInfo { FieldName = "WednesdayReg", ErrorMessage = Resource.AllocationMustNotExceedKronosHours, });
                }

                if ((DistributionThursdayRegularTotal + item.ThursdayReg) > KronosThursdayRegularTotal)
                {
                    ErrorList.Add(new ErrorInfo { FieldName = "ThursdayReg", ErrorMessage = Resource.AllocationMustNotExceedKronosHours, });
                }

                if ((DistributionFridayRegularTotal + item.FridayReg) > KronosFridayRegularTotal)
                {
                    ErrorList.Add(new ErrorInfo { FieldName = "FridayReg", ErrorMessage = Resource.AllocationMustNotExceedKronosHours, });
                }

                if ((DistributionSaturdayRegularTotal + item.SaturdayReg) > KronosSaturdayRegularTotal)
                {
                    ErrorList.Add(new ErrorInfo { FieldName = "SaturdayReg", ErrorMessage = Resource.AllocationMustNotExceedKronosHours, });
                }

                //Overtime hours not exempt validation
                if ((DistributionSundayOvertimeTotal + item.SundayOv) > KronosSundayOvertimeTotal)
                {
                    ErrorList.Add(new ErrorInfo { FieldName = "SundayOv", ErrorMessage = Resource.AllocationMustNotExceedKronosHours, });
                }

                if ((DistributionMondayOvertimeTotal + item.MondayOv) > KronosMondayOvertimeTotal)
                {
                    ErrorList.Add(new ErrorInfo { FieldName = "MondayOv", ErrorMessage = Resource.AllocationMustNotExceedKronosHours, });
                }

                if ((DistributionTuesdayOvertimeTotal + item.TuesdayOv) > KronosTuesdayOvertimeTotal)
                {
                    ErrorList.Add(new ErrorInfo { FieldName = "TuesdayOv", ErrorMessage = Resource.AllocationMustNotExceedKronosHours, });
                }

                if ((DistributionWednesdayOvertimeTotal + item.WednesdayOv) > KronosWednesdayOvertimeTotal)
                {
                    ErrorList.Add(new ErrorInfo { FieldName = "WednesdayOv", ErrorMessage = Resource.AllocationMustNotExceedKronosHours, });
                }

                if ((DistributionThursdayOvertimeTotal + item.ThursdayOv) > KronosThursdayOvertimeTotal)
                {
                    ErrorList.Add(new ErrorInfo { FieldName = "ThursdayOv", ErrorMessage = Resource.AllocationMustNotExceedKronosHours, });
                }

                if ((DistributionFridayOvertimeTotal + item.FridayOv) > KronosFridayOvertimeTotal)
                {
                    ErrorList.Add(new ErrorInfo { FieldName = "FridayOv", ErrorMessage = Resource.AllocationMustNotExceedKronosHours, });
                }

                if ((DistributionSaturdayOvertimeTotal + item.SaturdayOv) > KronosSaturdayOvertimeTotal)
                {
                    ErrorList.Add(new ErrorInfo { FieldName = "SaturdayOv", ErrorMessage = Resource.AllocationMustNotExceedKronosHours, });
                }
            }
        }

        public struct ErrorInfo
        {
            public string FieldName { get; set; }
            public string ErrorMessage { get; set; }
        }
    }
}

