﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Core
{
    class Helper
    {
        public static void GetPeriodDates(DateTime timesheetDate, out DateTime startDate, out DateTime endDate)
        {
            startDate = timesheetDate;
            if (startDate.DayOfWeek != DayOfWeek.Sunday)
            {
                do
                {
                    startDate = startDate.AddDays(-1);
                }
                while (startDate.DayOfWeek != DayOfWeek.Sunday);
            }

            endDate = startDate.AddDays(6);
        }
    }
}
