﻿using Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Core
{
    public class TimesheetPeriodLogModel
    {
        public int Id { get; set; }
        public int PeriodId { get; set; }
        public string Action { get; set; }
        public int LogEmployee { get; set; }
        public string Name { get; set; }
        public DateTime Date { get; set; }

        public string ActionString
        {
            get
            {
                switch (Action)
                {
                    case "created":
                        {
                            return Resource.Created;
                        }
                    case "submitted":
                        {
                            return Resource.Submitted;
                        }
                    case "approved":
                        {
                            return Resource.Approved;
                        }
                    case "senttosap":
                        {
                            return Resource.SentToSap;
                        }
                    case "revision":
                        {
                            return Resource.SentToRevision;
                        }                   
                    default:
                        {
                            return string.Empty;
                        }
                }
            }
        }



        public List<TimesheetPeriodLogModel> GetTimesheetPeriodLogList(int periodId)
        {
            List<string> actions = new List<string>();
            actions.Add("created");
            actions.Add("submitted");
            actions.Add("approved");
            actions.Add("senttosap");
            actions.Add("revision");

            var sssContext = new SSSEntities();
            var kronosContext = new KronosEntities();

            List<TimesheetPeriodLogModel> list = sssContext.TimesheetPeriodLogs.Where(p => p.TimesheetPeriodId == periodId && actions.Contains(p.Action)).Select(p => new TimesheetPeriodLogModel { Action = p.Action, Date = p.LogDate, Id = p.Id, LogEmployee = p.LogEmployeeId, PeriodId = p.TimesheetPeriodId }).OrderByDescending(p=>p.Date).ToList();

            PERSON person = null;
            foreach (TimesheetPeriodLogModel item in list)
            {
                person = kronosContext.PERSON.Where(p => p.PERSONID == item.LogEmployee).FirstOrDefault();
                if(person != null)
                {
                    item.Name = person.FULLNM;
                }
            }

            return list;
           
        }
    }
}
