﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Resources;

namespace Models.Core
{
    public class EmployeeInfoModel
    {
        string _exemptString = string.Empty;

        public int Id { get; set; }
        public int PersonId { get; set; }
        public string FullName { get; set; }
        public string SupervisorString { get; set; }
        public string EmployeeIdentification { get; set; }
        public bool Exempt { get; set; }
        public List<SupervisorInfoModel> SupervisorList { get; set; }
        public string CostCenterCode { get; set; }
        public string CostCenterDescription { get; set; }
        public int CostCenterId { get; set; }
        public string Company { get; set; }

        public string ExemptString
        {
            get
            {
                return _exemptString != null && _exemptString.ToLower() =="exempt"? Resource.Yes:"No";
            }

            set
            {
                _exemptString = value;
            }
        }


        public EmployeeInfoModel()
        {
            FullName = string.Empty;
            EmployeeIdentification = string.Empty;
            SupervisorList = new List<SupervisorInfoModel>();
        }

        public EmployeeInfoModel(int employeeId)
        {
            FullName = string.Empty;
            EmployeeIdentification = string.Empty;
            SupervisorList = new List<SupervisorInfoModel>();

            var kronosContext = new KronosEntities();
            PERSON emp = kronosContext.PERSON.Where(p => p.PERSONID == employeeId).FirstOrDefault();

            if (emp != null)
            {
                FullName = emp.FULLNM;
                EmployeeIdentification = emp.PERSONNUM;

                PERSONCSTMDATA customData = kronosContext.PERSONCSTMDATAs.Where(c => c.PERSONID == emp.PERSONID && c.CUSTOMDATADEFID == 7).FirstOrDefault();
                if (customData != null && customData.PERSONCSTMDATATXT.ToLower() == "exempt")
                {
                    Exempt = true;
                }

                List<SupervisorInfoModel> supervisorList = GetSupervisorList(employeeId);

                WTKEMPLOYEE wtkEmp = kronosContext.WTKEMPLOYEEs.Where(w => w.PERSONID == employeeId && w.EFFECTIVEDTM <= DateTime.Today && w.EXPIRATIONDTM >= DateTime.Today).FirstOrDefault();
                if (wtkEmp != null)
                {
                    foreach (SupervisorInfoModel item in supervisorList)
                    {
                        if (item.PersonId != wtkEmp.SUPERVISORID)
                        {
                            item.Delegate = true;
                            item.FullName = item.FullName + " (" + Resource.Delegate + ")";
                        }
                    }

                    PERSON supervisor = kronosContext.PERSON.Where(p => p.PERSONID == wtkEmp.SUPERVISORID).FirstOrDefault();
                    if (supervisor != null)
                    {
                        if (supervisorList.Select(x => x.PersonId == supervisor.PERSONID).Count() == 0)
                        {
                            supervisorList.Add(new SupervisorInfoModel { FullName = supervisor.FULLNM, PersonId = supervisor.PERSONID, PersonNum = supervisor.PERSONNUM });
                        }
                    }
                }

                SupervisorList = supervisorList;
                SupervisorString = string.Join(", ", SupervisorList.Select(s => s.FullName));

                GetCostCenterInfo(employeeId);

            }
        }

        public static PERSON GetPerson(int personId)
        {
            var kronosContext = new KronosEntities();
            return kronosContext.PERSON.Where(p => p.PERSONID == personId).FirstOrDefault();
        }

        public void GetCostCenterInfo(int employeeId)
        {
            var kronosContext = new KronosEntities();
            var sssContect = new SSSEntities();

            CostCenterCode = string.Empty;
            CostCenterDescription = string.Empty;
            Company = string.Empty;
            CostCenterId = 0;

            COMBHOMEACCT homeAcc = kronosContext.COMBHOMEACCTs.Where(h => h.EMPLOYEEID == employeeId && h.EFFECTIVEDTM <= DateTime.Today && h.EXPIRATIONDTM >= DateTime.Today).FirstOrDefault();
            if (homeAcc != null && homeAcc.LABORACCT != null && homeAcc.LABORACCT.LABORLEV3ID != null)
            {
                LABORLEVELENTRY entry = kronosContext.LABORLEVELENTRies.Where(e => e.LABORLEVELENTRYID == homeAcc.LABORACCT.LABORLEV3ID).FirstOrDefault();

                if (entry != null)
                {
                    if (!string.IsNullOrEmpty(entry.DESCRIPTION))
                    {
                        CostCenterDescription = entry.DESCRIPTION;
                    }

                    if (!string.IsNullOrEmpty(entry.NAME))
                    {
                        CostCenterCode = entry.NAME;
                    }

                    CostCenter costCenter = sssContect.CostCenters.Where(c => c.Code == CostCenterCode).FirstOrDefault();
                    if (costCenter != null)
                    {
                        CostCenterId = costCenter.Id;
                    }
                }

                entry = kronosContext.LABORLEVELENTRies.Where(e => e.LABORLEVELENTRYID == homeAcc.LABORACCT.LABORLEV1ID).FirstOrDefault();

                if (entry != null)
                {
                    if (!string.IsNullOrEmpty(entry.DESCRIPTION))
                    {
                        Company = entry.DESCRIPTION;
                    }
                }
            }

        }

        public List<SupervisorInfoModel> GetSupervisorList(int employeeId)
        {
            var kronosContext = new KronosEntities();
            return (from person in kronosContext.PERSON
                    join jaids in kronosContext.JAIDS on person.PERSONID equals jaids.PERSONID
                    join homeacc in kronosContext.COMBHOMEACCTs on jaids.EMPLOYEEID equals homeacc.EMPLOYEEID
                    join laboracc in kronosContext.LABORACCTs on homeacc.LABORACCTID equals laboracc.LABORACCTID
                    join ll in kronosContext.LLELABACCTSTMMs on laboracc.LABORLEV6ID equals ll.LABORLEVELENTRYID
                    join assign in kronosContext.PRSNACCSASSIGNs on ll.LABORACCTSETID equals assign.MGRACCESSLASID
                    join sup in kronosContext.PERSON on assign.PERSONID equals sup.PERSONID
                    where person.PERSONID == employeeId
                    && homeacc.EFFECTIVEDTM <= DateTime.Today && homeacc.EXPIRATIONDTM >= DateTime.Today
                    select new SupervisorInfoModel { FullName = sup.FULLNM, PersonId = sup.PERSONID, PersonNum = sup.PERSONNUM }).ToList();
        }

        public List<int> GetEmployeeIdsForSupervisor(int supervisorId)
        {
            var kronosContext = new KronosEntities();
            int mgrAccessId = 0;

            PRSNACCSASSIGN prsAccs = kronosContext.PRSNACCSASSIGNs.Where(p => p.PERSONID == supervisorId).FirstOrDefault();

            if (prsAccs != null && prsAccs.MGRACCESSLASID != null)
            {
                mgrAccessId = (int)prsAccs.MGRACCESSLASID;
            }

            var query = from ll in kronosContext.LASETLABACCTMMs
                        join c in kronosContext.COMBHOMEACCTs on ll.LABORACCTID equals c.LABORACCTID
                        join p in kronosContext.PERSON on c.EMPLOYEEID equals p.PERSONID
                        join s in kronosContext.PERSONSTATUSMMs on p.PERSONID equals s.PERSONID
                        where c.EFFECTIVEDTM <= DateTime.Today && c.EXPIRATIONDTM >= DateTime.Today
                        && s.EFFECTIVEDTM <= DateTime.Today && s.EXPIRATIONDTM >= DateTime.Today
                        && ll.LABORACCTSETID == mgrAccessId
                        select p.PERSONID;

            return query.ToList();
        }

        public List<EmployeeInfoModel> SearchEmployees(int supervisorId, string name, string id, DateTime? fromDate, DateTime? toDate, List<string> statusList)
        {
            var sssContext = new SSSEntities();
            var kronosContext = new KronosEntities();

            List<int> empIdList = GetEmployeeIdsForSupervisor(supervisorId);

            var query = from period in sssContext.TimesheetPeriods
                        where empIdList.Contains(period.EmployeeId)
                        select period;

            if (fromDate != null)
                query = query.Where(p => p.FromDate >= fromDate);

            if (toDate != null)
                query = query.Where(p => p.ToDate <= toDate);

            if (statusList != null && statusList.Count > 0)
                query = query.Where(p => statusList.Contains(p.Status));


            empIdList = query.Select(q => q.EmployeeId).Distinct().ToList();

            var result = from person in kronosContext.PERSON
                         join homeacc in kronosContext.COMBHOMEACCTs on person.PERSONID equals homeacc.EMPLOYEEID
                         join laboracc in kronosContext.LABORACCTs on homeacc.LABORACCTID equals laboracc.LABORACCTID
                         join custom in kronosContext.PERSONCSTMDATAs on new { person.PERSONID, CUSTOMDATADEFID = 7 } equals new { custom.PERSONID, custom.CUSTOMDATADEFID } into custgrp
                         from exemptgrp in custgrp.DefaultIfEmpty()

                         where empIdList.Contains(person.PERSONID)
                         && homeacc.EFFECTIVEDTM <= DateTime.Today && homeacc.EXPIRATIONDTM >= DateTime.Today
                         && person.SEARCHFULLNM.Contains(name)
                         && person.PERSONNUM.Contains(id)
                         select new EmployeeInfoModel
                         {
                             CostCenterDescription = laboracc.LABORLEV3NM + " (" + laboracc.LABORLEV3DSC + ")",
                             Company = laboracc.LABORLEV1DSC,
                             EmployeeIdentification = person.PERSONNUM,
                             FullName = person.SEARCHFULLNM,
                             ExemptString = exemptgrp.PERSONCSTMDATATXT,
                             PersonId = person.PERSONID
                         };

            List<EmployeeInfoModel> list = result.ToList();

            return list;
        }        
    }

    public class SupervisorInfoModel
    {
        public string FullName { get; set; }
        public int PersonId { get; set; }
        public string PersonNum { get; set; }
        public bool Delegate { get; set; }

        public static void GetSupervisorEmailInfo(int employeeId, List<string> emailList, List<string> supNameList)
        {
            List<SupervisorInfoModel> supervisors = new EmployeeInfoModel().GetSupervisorList(employeeId);
            foreach (SupervisorInfoModel item in supervisors)
            {
                PERSON sup = EmployeeInfoModel.GetPerson(item.PersonId);
                if (sup != null)
                {
                    EMAILADDRESS email = sup.EMAILADDRESSes.FirstOrDefault();
                    if (email != null && !string.IsNullOrEmpty(email.EMAILADDRESSTXT))
                    {
                        emailList.Add(email.EMAILADDRESSTXT);
                        supNameList.Add(sup.FULLNM);
                    }
                }
            }
        }
    }
}
