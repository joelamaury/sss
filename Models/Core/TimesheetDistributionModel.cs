﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Core
{
    public class TimesheetDistributionModel
    {
        public void CreateTimesheetDistribution(TimesheetGridModel item, int employeeId, DateTime timesheetDate, int loggedUserId)
        {
            var sssContext = new SSSEntities();
            TimesheetPeriod period = sssContext.TimesheetPeriods.Where(x => x.EmployeeId == employeeId && x.FromDate <= timesheetDate && x.ToDate >= timesheetDate).FirstOrDefault();
            if (period == null)
            {
                DateTime fromDate, toDate;
                Helper.GetPeriodDates(timesheetDate, out fromDate, out toDate);

                period = new TimesheetPeriod { EmployeeId = employeeId, FromDate = fromDate, LogDate = DateTime.Now, LogEmployeeId = loggedUserId, Status = "pendingsubmission", ToDate = toDate };
                period.TimesheetPeriodLogs.Add(new TimesheetPeriodLog { LogDate = DateTime.Now, LogEmployeeId = loggedUserId, Action = "created" });
                sssContext.TimesheetPeriods.Add(period);

                sssContext.SaveChanges();
            }

            TimesheetDistribution tsDist = new TimesheetDistribution();
            item.UpdateTImesheetDistribution(tsDist, employeeId, loggedUserId);
            tsDist.TimesheetPeriodId = period.Id;
            sssContext.TimesheetDistributions.Add(tsDist);

            sssContext.SaveChanges();

            sssContext.TimesheetPeriodLogs.Add(new TimesheetPeriodLog { LogDate = DateTime.Now, LogEmployeeId = loggedUserId, Action = "rowcreated", TimesheetPeriodId = tsDist.TimesheetPeriodId, TimesheetDistributionId = tsDist.Id });

            sssContext.SaveChanges();

            item.Id = tsDist.Id;
        }

        public void UpdateTimesheetDistribution(TimesheetGridModel item, int employeeId, DateTime timesheetDate, int loggedUserId)
        {
            var sssContext = new SSSEntities();

            TimesheetDistribution dist = sssContext.TimesheetDistributions.Where(d => d.Id == item.Id).FirstOrDefault();
            if (dist != null)
            {
                item.UpdateTImesheetDistribution(dist, employeeId, loggedUserId);

                sssContext.TimesheetPeriodLogs.Add(new TimesheetPeriodLog { LogDate = DateTime.Now, LogEmployeeId = loggedUserId, Action = "rowupdated", TimesheetPeriodId = dist.TimesheetPeriodId, TimesheetDistributionId = dist.Id });

                sssContext.SaveChanges();
            }
        }

        public void DeleteTimesheetDistribution(TimesheetGridModel item)
        {
            var sssContext = new SSSEntities();

            TimesheetDistribution dist = sssContext.TimesheetDistributions.Where(d => d.Id == item.Id).FirstOrDefault();
            if (dist != null)
            {
                List<TimesheetPeriodLog> logList = sssContext.TimesheetPeriodLogs.Where(x => x.TimesheetDistributionId == dist.Id).ToList();
                if(logList != null && logList.Count >0)
                {
                    sssContext.TimesheetPeriodLogs.RemoveRange(logList);
                }
              
                sssContext.TimesheetDistributions.Remove(dist);
                sssContext.SaveChanges();
            }
        }
    }
}
