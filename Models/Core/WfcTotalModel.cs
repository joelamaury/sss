﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Core
{
   public class WfcTotalModel
    {
        public int PayCodeId { get; set; }
        public string PayCodeName { get; set; }
        public bool Regular { get; set; }
        public bool Overtime { get; set; }
        public bool Display { get; set; }
        public decimal TimesheeTotal { get; set; }
        public DateTime TimesheetDate { get; set; }

        public List<WfcTotalModel> GetWfcTotal(int employeeId, DateTime startDate, DateTime endDate)
        {
            var kronosContext = new KronosEntities();
            var sssContext = new SSSEntities();

            List<WfcTotalModel> wfcList = (from wfcTotal in kronosContext.WFCTOTALs
                                           join paycode in kronosContext.PAYCODEs on wfcTotal.PAYCODEID equals paycode.PAYCODEID
                                           where wfcTotal.ADJAPPLYDTM.HasValue
                                           && wfcTotal.ADJAPPLYDTM.Value >= startDate
                                           && wfcTotal.ADJAPPLYDTM <= endDate
                                           && wfcTotal.EMPLOYEEID == employeeId
                                           && paycode.VISIBLE_TO_USER.HasValue
                                           && paycode.VISIBLE_TO_USER.Value == 1
                                           group wfcTotal by new { wfcTotal.ADJAPPLYDTM, paycode.PAYCODEID, paycode.NAME } into grp
                                           orderby grp.Key.ADJAPPLYDTM
                                           select new WfcTotalModel
                                           {
                                               TimesheetDate = grp.Key.ADJAPPLYDTM.Value,
                                               PayCodeId = grp.Key.PAYCODEID,
                                               PayCodeName = grp.Key.NAME,
                                               TimesheeTotal = grp.Sum(p => p.DURATIONSECSQTY.HasValue ? ((decimal)p.DURATIONSECSQTY.Value / 3600) : 0)
                                           }).ToList();

            List<PayCodeConfiguration> payCodeConfList = sssContext.PayCodeConfigurations.ToList();
            PayCodeConfiguration payCodeConf = null;
            foreach (WfcTotalModel item in wfcList.ToList())
            {
                item.TimesheeTotal = decimal.Round(item.TimesheeTotal, 2);
                payCodeConf = payCodeConfList.Where(p => p.KronosId == item.PayCodeId).FirstOrDefault();
                if (payCodeConf != null && (payCodeConf.Show || payCodeConf.Regular || payCodeConf.Overtime))
                {
                    item.Regular = payCodeConf.Regular;
                    item.Overtime = payCodeConf.Overtime;

                    if (item.Regular == true || item.Overtime == true)
                    {
                        item.Display = false;
                    }
                    else
                    {
                        item.Display = payCodeConf.Show;
                    }
                }
                else
                {
                    ///Wfctotal pay code is not configured to be shown in timesheet
                    wfcList.Remove(item);
                }
            }

            return wfcList;
        }

    }
}
