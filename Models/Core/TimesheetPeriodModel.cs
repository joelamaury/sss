﻿using Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Core
{
    public class TimesheetPeriodModel
    {
        public int PeriodId { get; set; }
        public int EmployeeId { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string Status { get; set; }
        public DateTime LogDate { get; set; }
        public string StatusString
        {
            get
            {
                switch (Status)
                {
                    case "notcreated":
                        {
                            return Resource.NotCreated;
                        }
                    case "pendingsubmission":
                        {
                            return Resource.PendingToSubmit;
                        }
                    case "submitted":
                        {
                            return Resource.Submitted;
                        }
                    case "approved":
                        {
                            return Resource.Approved;
                        }
                    case "senttosap":
                        {
                            return Resource.SentToSap;
                        }
                    case "revision":
                        {
                            return Resource.SentToRevision;
                        }
                    default:
                        {
                            return string.Empty;
                        }
                }
            }
        }


        public TimesheetPeriod GetTimesheetPeriod(int employeeId, DateTime timesheetDate)
        {
            var sssContext = new SSSEntities();
            return sssContext.TimesheetPeriods.Where(d => d.EmployeeId == employeeId && d.FromDate <= timesheetDate && d.ToDate >= timesheetDate).FirstOrDefault();
        }

        public List<TimesheetPeriod> GetTimesheetPeriod(int employeeId, DateTime fromDate, DateTime toDate)
        {
            var sssContext = new SSSEntities();
            return sssContext.TimesheetPeriods.Where(d => d.EmployeeId == employeeId && d.FromDate >= fromDate && d.ToDate <= toDate).ToList();
        }

        public List<TimesheetPeriodModel> GetTimesheetPeriod(int employeeId)
        {
            var sssContext = new SSSEntities();
            return sssContext.TimesheetPeriods.Where(d => d.EmployeeId == employeeId).Select(s => new TimesheetPeriodModel { EmployeeId= s.EmployeeId, FromDate = s.FromDate, ToDate = s.ToDate, LogDate = s.LogDate, PeriodId = s.Id, Status = s.Status }).OrderByDescending(p=>p.FromDate).ToList();
        }

        public TimesheetPeriod Update(int employeeId, DateTime timesheetDate, int loggedUserId, string status)
        {
            var sssContext = new SSSEntities();
            TimesheetPeriod period = sssContext.TimesheetPeriods.Where(d => d.EmployeeId == employeeId && d.FromDate <= timesheetDate && d.ToDate >= timesheetDate).FirstOrDefault();
            period.Status =status;
            period.TimesheetPeriodLogs.Add(new TimesheetPeriodLog { LogDate = DateTime.Now, LogEmployeeId = loggedUserId, Action = status });
            sssContext.SaveChanges();
            return period;
        }
    }
}
