﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Core
{
    public class TimesheetGridModel
    {
        public int Id { get; set; }
        public int ConceptId { get; set; }
        public int? CostCenterId { get; set; }
        public int? InternalOrderId { get; set; }
        public decimal SundayReg { get; set; }
        public decimal SundayOv { get; set; }
        public decimal MondayReg { get; set; }
        public decimal MondayOv { get; set; }
        public decimal TuesdayReg { get; set; }
        public decimal TuesdayOv { get; set; }
        public decimal WednesdayReg { get; set; }
        public decimal WednesdayOv { get; set; }
        public decimal ThursdayReg { get; set; }
        public decimal ThursdayOv { get; set; }
        public decimal FridayReg { get; set; }
        public decimal FridayOv { get; set; }
        public decimal SaturdayReg { get; set; }
        public decimal SaturdayOv { get; set; }
        public string Status { get; set; }
        public int TaskDescriptions { get; set; }
        public string Type { get; set; }
        public int PayCodeId { get; set; }
        public string PayCodeName { get; set; }
        public string Note { get; set; }

        public bool RowForHeaderOnly { get; set; }

        public decimal TotalReg
        {
            get
            {
                return SundayReg + MondayReg + TuesdayReg + WednesdayReg + ThursdayReg + FridayReg + SaturdayReg;
            }
        }

        public decimal TotalOv
        {
            get
            {
                return SundayOv + MondayOv + TuesdayOv + WednesdayOv + ThursdayOv + FridayOv + SaturdayOv;
            }
        }

        public decimal GrandTotal
        {
            get
            {
                return SundayReg + MondayReg + TuesdayReg + WednesdayReg + ThursdayReg + FridayReg + SaturdayReg +
                      SundayOv + MondayOv + TuesdayOv + WednesdayOv + ThursdayOv + FridayOv + SaturdayOv;
            }
        }

        public string SundayHeader { get; set; }
        public string MondayHeader { get; set; }
        public string TuesdayHeader { get; set; }
        public string WednesdayHeader { get; set; }
        public string ThursdayHeader { get; set; }
        public string FridayHeader { get; set; }
        public string SaturdayHeader { get; set; }

        public string FromDateString { get; set; }
        public string ToDateString { get; set; }

        public void UpdateTImesheetDistribution(TimesheetDistribution tsDist, int employeeId, int loggedUserId)
        {
            tsDist.ConceptId = ConceptId;
            tsDist.CostCenterId = CostCenterId;
            tsDist.EmployeeId = employeeId;
            tsDist.InternalOrderId = InternalOrderId;
            tsDist.LogDate = DateTime.Now;
            tsDist.LogEmployeeId = loggedUserId;

            tsDist.SundayRegular = SundayReg;
            tsDist.MondayRegular = MondayReg;
            tsDist.TuesdayRegular = TuesdayReg;
            tsDist.WednesdayRegular = WednesdayReg;
            tsDist.ThursdayRegular = ThursdayReg;
            tsDist.FridayRegular = FridayReg;
            tsDist.SaturdayRegular = SaturdayReg;

            tsDist.SundayOvertime = SundayOv;
            tsDist.MondayOvertime = MondayOv;
            tsDist.TuesdayOvertime = TuesdayOv;
            tsDist.WednesdayOvertime = WednesdayOv;
            tsDist.ThursdayOvertime = ThursdayOv;
            tsDist.FridayOvertime = FridayOv;
            tsDist.SaturdayOvertime = SaturdayOv;
            tsDist.Note = Note;
        }

        public List<TimesheetGridModel> GetKronosTimesheetData(int employeeId, DateTime timesheetDate)
        {
            int regularDefaultPayCodeId = 0;

            var sssContext = new SSSEntities();
            PayCodeConfiguration regularDefault = sssContext.PayCodeConfigurations.Where(p => p.RegularDefault).FirstOrDefault();
            if (regularDefault != null)
            {
                regularDefaultPayCodeId = regularDefault.KronosId;
            }

            DateTime startDate, endDate;
            Helper.GetPeriodDates(timesheetDate, out startDate, out endDate);

            WfcTotalModel wfcTotal = new WfcTotalModel();
            List<WfcTotalModel> wfcList = wfcTotal.GetWfcTotal(employeeId, startDate, endDate);

            List<TimesheetGridModel> list = new List<TimesheetGridModel>();

            if (regularDefaultPayCodeId != 0)
            {
                list.Add(new TimesheetGridModel
                {
                    Id = 1,
                    RowForHeaderOnly = true,
                    PayCodeId = regularDefaultPayCodeId,
                    PayCodeName = "Regular/Overtime",
                    SundayHeader = startDate.ToString(CultureInfo.CurrentCulture.ToString().ToLower() == "en-us" ? "MMM dd yyyy" : "dd MMM yyyy", CultureInfo.CurrentCulture),
                    MondayHeader = startDate.AddDays(1).ToString(CultureInfo.CurrentCulture.ToString().ToLower() == "en-us" ? "MMM dd yyyy" : "dd MMM yyyy", CultureInfo.CurrentCulture),
                    TuesdayHeader = startDate.AddDays(2).ToString(CultureInfo.CurrentCulture.ToString().ToLower() == "en-us" ? "MMM dd yyyy" : "dd MMM yyyy", CultureInfo.CurrentCulture),
                    WednesdayHeader = startDate.AddDays(3).ToString(CultureInfo.CurrentCulture.ToString().ToLower() == "en-us" ? "MMM dd yyyy" : "dd MMM yyyy", CultureInfo.CurrentCulture),
                    ThursdayHeader = startDate.AddDays(4).ToString(CultureInfo.CurrentCulture.ToString().ToLower() == "en-us" ? "MMM dd yyyy" : "dd MMM yyyy", CultureInfo.CurrentCulture),
                    FridayHeader = startDate.AddDays(5).ToString(CultureInfo.CurrentCulture.ToString().ToLower() == "en-us" ? "MMM dd yyyy" : "dd MMM yyyy", CultureInfo.CurrentCulture),
                    SaturdayHeader = startDate.AddDays(6).ToString(CultureInfo.CurrentCulture.ToString().ToLower() == "en-us" ? "MMM dd yyyy" : "dd MMM yyyy", CultureInfo.CurrentCulture),
                    FromDateString = startDate.ToString(CultureInfo.CurrentCulture.ToString().ToLower() == "en-us" ? "MMM dd yyyy" : "dd MMM yyyy", CultureInfo.CurrentCulture),
                    ToDateString = endDate.ToString(CultureInfo.CurrentCulture.ToString().ToLower() == "en-us" ? "MMM dd yyyy" : "dd MMM yyyy", CultureInfo.CurrentCulture)
                });

                int index = 2;
                foreach (var item in wfcList.Where(s => s.Display).Select(s => new { Name = s.PayCodeName, PcId = s.PayCodeId }).Distinct().ToList())
                {
                    list.Add(new TimesheetGridModel
                    {
                        Id = index,
                        PayCodeName = item.Name,
                        PayCodeId = item.PcId
                    });
                    index++;
                }

                if (wfcList.Count > 0)
                {
                    foreach (TimesheetGridModel item in list)
                    {
                        item.RowForHeaderOnly = false;
                        //Regular and Overtime Hours
                        if (item.Id == 1)
                        {
                            item.SundayReg = wfcList.Where(w => w.TimesheetDate.DayOfWeek == DayOfWeek.Sunday && w.Regular).Sum(p => p.TimesheeTotal);
                            item.MondayReg = wfcList.Where(w => w.TimesheetDate.DayOfWeek == DayOfWeek.Monday && w.Regular).Sum(p => p.TimesheeTotal);
                            item.TuesdayReg = wfcList.Where(w => w.TimesheetDate.DayOfWeek == DayOfWeek.Tuesday && w.Regular).Sum(p => p.TimesheeTotal);
                            item.WednesdayReg = wfcList.Where(w => w.TimesheetDate.DayOfWeek == DayOfWeek.Wednesday && w.Regular).Sum(p => p.TimesheeTotal);
                            item.ThursdayReg = wfcList.Where(w => w.TimesheetDate.DayOfWeek == DayOfWeek.Thursday && w.Regular).Sum(p => p.TimesheeTotal);
                            item.FridayReg = wfcList.Where(w => w.TimesheetDate.DayOfWeek == DayOfWeek.Friday && w.Regular).Sum(p => p.TimesheeTotal);
                            item.SaturdayReg = wfcList.Where(w => w.TimesheetDate.DayOfWeek == DayOfWeek.Saturday && w.Regular).Sum(p => p.TimesheeTotal);

                            item.SundayOv = wfcList.Where(w => w.TimesheetDate.DayOfWeek == DayOfWeek.Sunday && w.Overtime).Sum(p => p.TimesheeTotal);
                            item.MondayOv = wfcList.Where(w => w.TimesheetDate.DayOfWeek == DayOfWeek.Monday && w.Overtime).Sum(p => p.TimesheeTotal);
                            item.TuesdayOv = wfcList.Where(w => w.TimesheetDate.DayOfWeek == DayOfWeek.Tuesday && w.Overtime).Sum(p => p.TimesheeTotal);
                            item.WednesdayOv = wfcList.Where(w => w.TimesheetDate.DayOfWeek == DayOfWeek.Wednesday && w.Overtime).Sum(p => p.TimesheeTotal);
                            item.ThursdayOv = wfcList.Where(w => w.TimesheetDate.DayOfWeek == DayOfWeek.Thursday && w.Overtime).Sum(p => p.TimesheeTotal);
                            item.FridayOv = wfcList.Where(w => w.TimesheetDate.DayOfWeek == DayOfWeek.Friday && w.Overtime).Sum(p => p.TimesheeTotal);
                            item.SaturdayOv = wfcList.Where(w => w.TimesheetDate.DayOfWeek == DayOfWeek.Saturday && w.Overtime).Sum(p => p.TimesheeTotal);
                        }
                        else
                        {
                            item.SundayReg = wfcList.Where(w => w.TimesheetDate.DayOfWeek == DayOfWeek.Sunday && w.PayCodeId == item.PayCodeId).Sum(p => p.TimesheeTotal);
                            item.MondayReg = wfcList.Where(w => w.TimesheetDate.DayOfWeek == DayOfWeek.Monday && w.PayCodeId == item.PayCodeId).Sum(p => p.TimesheeTotal);
                            item.TuesdayReg = wfcList.Where(w => w.TimesheetDate.DayOfWeek == DayOfWeek.Tuesday && w.PayCodeId == item.PayCodeId).Sum(p => p.TimesheeTotal);
                            item.WednesdayReg = wfcList.Where(w => w.TimesheetDate.DayOfWeek == DayOfWeek.Wednesday && w.PayCodeId == item.PayCodeId).Sum(p => p.TimesheeTotal);
                            item.ThursdayReg = wfcList.Where(w => w.TimesheetDate.DayOfWeek == DayOfWeek.Thursday && w.PayCodeId == item.PayCodeId).Sum(p => p.TimesheeTotal);
                            item.FridayReg = wfcList.Where(w => w.TimesheetDate.DayOfWeek == DayOfWeek.Friday && w.PayCodeId == item.PayCodeId).Sum(p => p.TimesheeTotal);
                            item.SaturdayReg = wfcList.Where(w => w.TimesheetDate.DayOfWeek == DayOfWeek.Saturday && w.PayCodeId == item.PayCodeId).Sum(p => p.TimesheeTotal);

                        }
                    }
                }
            }
            return list;
        }

        public List<TimesheetGridModel> GetDistributionTimesheetData(int employeeId, DateTime timesheetDate)
        {
            var sssContext = new SSSEntities();

            DateTime startDate;
            DateTime endDate;
            Helper.GetPeriodDates(timesheetDate, out startDate, out endDate);

            string SundayHeader = startDate.ToString(CultureInfo.CurrentCulture.ToString().ToLower() == "en-us" ? "MMM dd yyyy" : "dd MMM yyyy", CultureInfo.CurrentCulture);
            string MondayHeader = startDate.AddDays(1).ToString(CultureInfo.CurrentCulture.ToString().ToLower() == "en-us" ? "MMM dd yyyy" : "dd MMM yyyy", CultureInfo.CurrentCulture);
            string TuesdayHeader = startDate.AddDays(2).ToString(CultureInfo.CurrentCulture.ToString().ToLower() == "en-us" ? "MMM dd yyyy" : "dd MMM yyyy", CultureInfo.CurrentCulture);
            string WednesdayHeader = startDate.AddDays(3).ToString(CultureInfo.CurrentCulture.ToString().ToLower() == "en-us" ? "MMM dd yyyy" : "dd MMM yyyy", CultureInfo.CurrentCulture);
            string ThursdayHeader = startDate.AddDays(4).ToString(CultureInfo.CurrentCulture.ToString().ToLower() == "en-us" ? "MMM dd yyyy" : "dd MMM yyyy", CultureInfo.CurrentCulture);
            string FridayHeader = startDate.AddDays(5).ToString(CultureInfo.CurrentCulture.ToString().ToLower() == "en-us" ? "MMM dd yyyy" : "dd MMM yyyy", CultureInfo.CurrentCulture);
            string SaturdayHeader = startDate.AddDays(6).ToString(CultureInfo.CurrentCulture.ToString().ToLower() == "en-us" ? "MMM dd yyyy" : "dd MMM yyyy", CultureInfo.CurrentCulture);

            List<TimesheetGridModel> list = new List<TimesheetGridModel>();
            TimesheetGridModel tsModel = null;
            TimesheetPeriod period = sssContext.TimesheetPeriods.Where(d => d.EmployeeId == employeeId && d.FromDate <= timesheetDate && d.ToDate >= timesheetDate).FirstOrDefault();
            if (period != null && period.TimesheetDistributions.Count > 0)
            {
                foreach (TimesheetDistribution item in period.TimesheetDistributions)
                {
                    tsModel = new TimesheetGridModel();
                    tsModel.Id = item.Id;
                    tsModel.ConceptId = item.ConceptId;
                    tsModel.InternalOrderId = item.InternalOrderId;
                    tsModel.CostCenterId = item.CostCenterId;

                    tsModel.SundayReg = item.SundayRegular;
                    tsModel.SundayOv = item.SundayOvertime;
                    tsModel.MondayReg = item.MondayRegular;
                    tsModel.MondayOv = item.MondayOvertime;
                    tsModel.TuesdayReg = item.TuesdayRegular;
                    tsModel.TuesdayOv = item.TuesdayOvertime;
                    tsModel.WednesdayReg = item.WednesdayRegular;
                    tsModel.WednesdayOv = item.WednesdayOvertime;
                    tsModel.ThursdayReg = item.ThursdayRegular;
                    tsModel.ThursdayOv = item.ThursdayOvertime;
                    tsModel.FridayReg = item.FridayRegular;
                    tsModel.FridayOv = item.FridayOvertime;
                    tsModel.SaturdayReg = item.SaturdayRegular;
                    tsModel.SaturdayOv = item.SaturdayOvertime;
                    tsModel.Note = item.Note;

                    list.Add(tsModel);
                }
            }
            else
            {
                tsModel = new TimesheetGridModel();
                tsModel.Id = 0;

                WfcTotalModel wfcTotal = new WfcTotalModel();
                List<WfcTotalModel> wfcList = wfcTotal.GetWfcTotal(employeeId, startDate, endDate);

                tsModel.SundayReg = wfcList.Where(w => w.TimesheetDate.DayOfWeek == DayOfWeek.Sunday && w.Regular).Sum(p => p.TimesheeTotal);
                tsModel.MondayReg = wfcList.Where(w => w.TimesheetDate.DayOfWeek == DayOfWeek.Monday && w.Regular).Sum(p => p.TimesheeTotal);
                tsModel.TuesdayReg = wfcList.Where(w => w.TimesheetDate.DayOfWeek == DayOfWeek.Tuesday && w.Regular).Sum(p => p.TimesheeTotal);
                tsModel.WednesdayReg = wfcList.Where(w => w.TimesheetDate.DayOfWeek == DayOfWeek.Wednesday && w.Regular).Sum(p => p.TimesheeTotal);
                tsModel.ThursdayReg = wfcList.Where(w => w.TimesheetDate.DayOfWeek == DayOfWeek.Thursday && w.Regular).Sum(p => p.TimesheeTotal);
                tsModel.FridayReg = wfcList.Where(w => w.TimesheetDate.DayOfWeek == DayOfWeek.Friday && w.Regular).Sum(p => p.TimesheeTotal);
                tsModel.SaturdayReg = wfcList.Where(w => w.TimesheetDate.DayOfWeek == DayOfWeek.Saturday && w.Regular).Sum(p => p.TimesheeTotal);

                tsModel.SundayOv = wfcList.Where(w => w.TimesheetDate.DayOfWeek == DayOfWeek.Sunday && w.Overtime).Sum(p => p.TimesheeTotal);
                tsModel.MondayOv = wfcList.Where(w => w.TimesheetDate.DayOfWeek == DayOfWeek.Monday && w.Overtime).Sum(p => p.TimesheeTotal);
                tsModel.TuesdayOv = wfcList.Where(w => w.TimesheetDate.DayOfWeek == DayOfWeek.Tuesday && w.Overtime).Sum(p => p.TimesheeTotal);
                tsModel.WednesdayOv = wfcList.Where(w => w.TimesheetDate.DayOfWeek == DayOfWeek.Wednesday && w.Overtime).Sum(p => p.TimesheeTotal);
                tsModel.ThursdayOv = wfcList.Where(w => w.TimesheetDate.DayOfWeek == DayOfWeek.Thursday && w.Overtime).Sum(p => p.TimesheeTotal);
                tsModel.FridayOv = wfcList.Where(w => w.TimesheetDate.DayOfWeek == DayOfWeek.Friday && w.Overtime).Sum(p => p.TimesheeTotal);
                tsModel.SaturdayOv = wfcList.Where(w => w.TimesheetDate.DayOfWeek == DayOfWeek.Saturday && w.Overtime).Sum(p => p.TimesheeTotal);

                list.Add(tsModel);
            }

            if (list.Count > 0)
            {
                list[0].SundayHeader = SundayHeader;
                list[0].MondayHeader = MondayHeader;
                list[0].TuesdayHeader = TuesdayHeader;
                list[0].WednesdayHeader = WednesdayHeader;
                list[0].ThursdayHeader = ThursdayHeader;
                list[0].FridayHeader = FridayHeader;
                list[0].SaturdayHeader = SaturdayHeader;
                list[0].FromDateString = startDate.ToString(CultureInfo.CurrentCulture.ToString().ToLower() == "en-us" ? "MMM dd yyyy" : "dd MMM yyyy", CultureInfo.CurrentCulture);
                list[0].ToDateString = endDate.ToString(CultureInfo.CurrentCulture.ToString().ToLower() == "en-us" ? "MMM dd yyyy" : "dd MMM yyyy", CultureInfo.CurrentCulture);
            }

            return list;
        }

        public void AutoDistributeKronosTimesheet(List<TimesheetGridModel> kronosTimesheet, int employeeId, DateTime timesheetDate, int loggedUserId, bool? overwrite)
        {
            EmployeeInfoModel empInfo = new EmployeeInfoModel();
            empInfo.GetCostCenterInfo(employeeId);

            if (kronosTimesheet.Where(t => t.RowForHeaderOnly == false).Count() == 0)
            {
                return;
            }

            int? empCostCenterId = null;
            if (empInfo.CostCenterId != 0)
            {
                empCostCenterId = empInfo.CostCenterId;
            }

            var sssContext = new SSSEntities();
            TimesheetPeriod period = sssContext.TimesheetPeriods.Where(x => x.EmployeeId == employeeId && x.FromDate <= timesheetDate && x.ToDate >= timesheetDate).FirstOrDefault();
            if (period == null || period.Status =="notcreated")
            {
                DateTime fromDate, toDate;
                Helper.GetPeriodDates(timesheetDate, out fromDate, out toDate);

                period = new TimesheetPeriod { EmployeeId = employeeId, FromDate = fromDate, LogDate = DateTime.Now, LogEmployeeId = loggedUserId, Status = "pendingsubmission", ToDate = toDate };
                period.TimesheetPeriodLogs.Add(new TimesheetPeriodLog { LogDate = DateTime.Now, LogEmployeeId = loggedUserId, Action = "created" });
                sssContext.TimesheetPeriods.Add(period);

                sssContext.SaveChanges();

                TimesheetDistribution tsDist = null;
                foreach (TimesheetGridModel item in kronosTimesheet)
                {
                    if (item.GrandTotal > 0)
                    {
                        tsDist = new TimesheetDistribution();
                        tsDist.ConceptId = item.PayCodeId;
                        tsDist.CostCenterId = empCostCenterId;
                        tsDist.EmployeeId = employeeId;
                        tsDist.LogDate = DateTime.Now;
                        tsDist.LogEmployeeId = loggedUserId;

                        tsDist.SundayRegular = item.SundayReg;
                        tsDist.SundayOvertime = item.SundayOv;
                        tsDist.MondayRegular = item.MondayReg;
                        tsDist.MondayOvertime = item.MondayOv;
                        tsDist.TuesdayRegular = item.TuesdayReg;
                        tsDist.TuesdayOvertime = item.TuesdayOv;
                        tsDist.WednesdayRegular = item.WednesdayReg;
                        tsDist.WednesdayOvertime = item.WednesdayOv;
                        tsDist.ThursdayRegular = item.ThursdayReg;
                        tsDist.ThursdayOvertime = item.ThursdayOv;
                        tsDist.FridayRegular = item.FridayReg;
                        tsDist.FridayOvertime = item.FridayOv;
                        tsDist.SaturdayRegular = item.SaturdayReg;
                        tsDist.SaturdayOvertime = item.SaturdayOv;

                        tsDist.TimesheetPeriodLogs.Add(new TimesheetPeriodLog { LogDate = DateTime.Now, LogEmployeeId = loggedUserId, Action = "rowcreated", TimesheetPeriodId = period.Id });

                        period.TimesheetDistributions.Add(tsDist);
                    }
                }

                sssContext.SaveChanges();
            }
            else
            {
                if(overwrite !=null && overwrite == true)
                {
                    period.TimesheetPeriodLogs.Add(new TimesheetPeriodLog { LogDate = DateTime.Now, LogEmployeeId = loggedUserId, Action = "overwrite" });

                    foreach (TimesheetDistribution dist in period.TimesheetDistributions.ToList())
                    {
                        List<TimesheetPeriodLog> logs = sssContext.TimesheetPeriodLogs.Where(p => p.TimesheetDistributionId == dist.Id).ToList();
                        if(logs.Count>0)
                        {
                            sssContext.TimesheetPeriodLogs.RemoveRange(logs);
                        }

                        sssContext.TimesheetDistributions.Remove(dist);
                    }

                    TimesheetDistribution tsDist = null;
                    foreach (TimesheetGridModel item in kronosTimesheet)
                    {
                        if (item.GrandTotal > 0)
                        {
                            tsDist = new TimesheetDistribution();
                            tsDist.ConceptId = item.PayCodeId;
                            tsDist.CostCenterId = empCostCenterId;
                            tsDist.EmployeeId = employeeId;
                            tsDist.LogDate = DateTime.Now;
                            tsDist.LogEmployeeId = loggedUserId;

                            tsDist.SundayRegular = item.SundayReg;
                            tsDist.SundayOvertime = item.SundayOv;
                            tsDist.MondayRegular = item.MondayReg;
                            tsDist.MondayOvertime = item.MondayOv;
                            tsDist.TuesdayRegular = item.TuesdayReg;
                            tsDist.TuesdayOvertime = item.TuesdayOv;
                            tsDist.WednesdayRegular = item.WednesdayReg;
                            tsDist.WednesdayOvertime = item.WednesdayOv;
                            tsDist.ThursdayRegular = item.ThursdayReg;
                            tsDist.ThursdayOvertime = item.ThursdayOv;
                            tsDist.FridayRegular = item.FridayReg;
                            tsDist.FridayOvertime = item.FridayOv;
                            tsDist.SaturdayRegular = item.SaturdayReg;
                            tsDist.SaturdayOvertime = item.SaturdayOv;

                            tsDist.TimesheetPeriodLogs.Add(new TimesheetPeriodLog { LogDate = DateTime.Now, LogEmployeeId = loggedUserId, Action = "rowcreated", TimesheetPeriodId = period.Id });

                            period.TimesheetDistributions.Add(tsDist);
                        }
                    }

                    sssContext.SaveChanges();


                }
            }
        }
    }
}
