//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class TimesheetPeriodLog
    {
        public int Id { get; set; }
        public int TimesheetPeriodId { get; set; }
        public Nullable<int> TimesheetDistributionId { get; set; }
        public string Action { get; set; }
        public int LogEmployeeId { get; set; }
        public System.DateTime LogDate { get; set; }
    
        public virtual TimesheetPeriod TimesheetPeriod { get; set; }
        public virtual TimesheetDistribution TimesheetDistribution { get; set; }
    }
}
