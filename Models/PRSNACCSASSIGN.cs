//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class PRSNACCSASSIGN
    {
        public int PERSONID { get; set; }
        public int TRANSFEMPLOYEESW { get; set; }
        public Nullable<int> SSETRANSFERLASID { get; set; }
        public Nullable<int> MGRTRANSFERLASID { get; set; }
        public Nullable<int> MGRACCESSLASID { get; set; }
        public Nullable<int> ACCESSPROFID { get; set; }
        public Nullable<int> PREFERENCEPROFID { get; set; }
        public Nullable<int> MGRWORKRULEDAPID { get; set; }
        public Nullable<int> SSEWORKRULEDAPID { get; set; }
        public Nullable<int> MGRPAYCODEDAPID { get; set; }
        public Nullable<int> SSEPAYCODEDAPID { get; set; }
        public Nullable<int> REPORTDAPID { get; set; }
        public Nullable<int> TIMEENTRYTYPEID { get; set; }
        public Nullable<int> SCHEDPATTRNDAPID { get; set; }
        public Nullable<int> MGRSHFTCDDAPID { get; set; }
        public Nullable<int> SSESHFTCDDAPID { get; set; }
        public Nullable<int> GROUPSCHEDDAPID { get; set; }
        public Nullable<int> AVLBLTYPTRNDAPID { get; set; }
        public Nullable<int> MGRWORKFLOWDAPID { get; set; }
        public Nullable<int> SSEWORKFLOWDAPID { get; set; }
        public Nullable<int> MGRVIEWPAYCODDAPID { get; set; }
        public Nullable<int> DELEGATEDAPID { get; set; }
        public int VERSIONCNT { get; set; }
        public int ISOTAPPROVERSW { get; set; }
        public Nullable<int> HFQUERYDAPID { get; set; }
        public Nullable<int> LOCALEPROFILEID { get; set; }
        public Nullable<int> MGRAPPROVALLASID { get; set; }
        public Nullable<int> NOTIFPROFILEID { get; set; }
        public Nullable<int> GAEVENTSUBTYPEDAPID { get; set; }
        public Nullable<int> KNOWNPLACEPROFID { get; set; }
        public Nullable<int> ACCESSMETHODPROFID { get; set; }
    
        public virtual PERSON PERSON { get; set; }
    }
}
