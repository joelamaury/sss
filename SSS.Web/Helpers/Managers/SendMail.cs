﻿using NLog;
using SSS.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;

namespace SSS.Helpers
{
    public class SendMail
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public static void SendMailMessage(String strTo, String[] strBcc, String strSubject, String strBody, SmtpSettings smtpSettings)
        {
            try
            {
                String[] sendTo = strBcc;
                MailMessage oEmail = new MailMessage();
                oEmail.From = new System.Net.Mail.MailAddress(smtpSettings.Account);
                oEmail.To.Add(strTo);
                oEmail.Subject = strSubject;
                oEmail.Body = strBody;
                oEmail.IsBodyHtml = true;
                oEmail.Priority = System.Net.Mail.MailPriority.Normal;

                if (sendTo.Length > 0)
                {
                    for (int i = 0; i < sendTo.Length; i++)
                        if (!String.IsNullOrEmpty(sendTo[i]))
                            oEmail.CC.Add(sendTo[i]);
                }

                SmtpClient oCliente = new SmtpClient();
                oCliente.Host = smtpSettings.Server;
                oCliente.Port = smtpSettings.Port;

                if (!String.IsNullOrEmpty(smtpSettings.Account) && !String.IsNullOrEmpty(smtpSettings.Password))
                {
                    oCliente.Credentials = new System.Net.NetworkCredential(smtpSettings.Account, smtpSettings.Password);
                    oCliente.EnableSsl = smtpSettings.UseSsl;
                    oCliente.Timeout = smtpSettings.Timeout;
                }

                oCliente.Send(oEmail);
            }
            catch (Exception ex)
            {
                logger.Fatal(ex);
                throw (ex);
            }
        }
    }
}
