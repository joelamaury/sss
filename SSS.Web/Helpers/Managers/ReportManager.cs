﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using NLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace SSS.Helpers
{
    public class ReportManager
    {
        #region initialization
        //Singleton object creation
        private static ReportManager instance;

        private static Logger logger = LogManager.GetCurrentClassLogger();
        private static object syncRoot = new Object();

        string reportPath = Path.Combine(Environment.CurrentDirectory, "Views/Reports/");

        private ReportManager() { }

        public static ReportManager Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                            instance = new ReportManager();
                    }
                }
                return instance;
            }
        }
        #endregion

        public ReportDocument LaunchInstantReportByReportName(string reportName, string ConnectionString, string param, int value)
        {

            ReportDocument crystalReport = new ReportDocument();
            string ReportLoad = Path.Combine(reportPath + reportName);

            logger.Error(string.Format("Path->{0}", ReportLoad));

            crystalReport.Load(ReportLoad);

            logger.Error(string.Format("Read Report->{0}", DateTime.Now));

            SubreportObject subReport;

            logger.Error(string.Format("Read Tables->{0}", DateTime.Now));

            foreach (Table reportTable in crystalReport.Database.Tables)
            {
                reportTable.LogOnInfo.ConnectionInfo = GetConnection(ConnectionString);
                reportTable.ApplyLogOnInfo(reportTable.LogOnInfo);
            }


            logger.Error(string.Format("Read section->{0}", DateTime.Now));
            foreach (CrystalDecisions.CrystalReports.Engine.Section section in crystalReport.ReportDefinition.Sections)
                foreach (ReportObject sectionReport in section.ReportObjects)
                {
                    if (sectionReport.Kind != ReportObjectKind.SubreportObject)
                        continue;
                    subReport = (SubreportObject)sectionReport;
                    ReportDocument sectionSubReport = subReport.OpenSubreport(subReport.Name);
                    if (sectionSubReport.Database != null)
                        foreach (Table subrptTable in sectionSubReport.Database.Tables)
                        {
                            subrptTable.LogOnInfo.ConnectionInfo = GetConnection(ConnectionString);
                            subrptTable.ApplyLogOnInfo(subrptTable.LogOnInfo);
                        }
                }

            logger.Error(string.Format("Set Parameter->{0}", DateTime.Now));
            crystalReport.SetParameterValue(param, value);

            return crystalReport;
        }

        #region Generacion de Reportes
        private ConnectionInfo GetConnection(string ConnectionString)
        {
            System.Data.Common.DbConnectionStringBuilder connectionString = new System.Data.Common.DbConnectionStringBuilder { ConnectionString = ConnectionString };
            object userId, pass, db, server;
            connectionString.TryGetValue("user id", out userId);
            connectionString.TryGetValue("password", out pass);
            connectionString.TryGetValue("data source", out server);
            connectionString.TryGetValue("initial catalog", out db);

            return new ConnectionInfo
            {
                ServerName = server.ToString(),
                DatabaseName = db.ToString(),
                UserID = userId.ToString(),
                Password = pass.ToString(),
            };

        }

        private void SaveFileStream(String path, Stream stream)
        {
            var fileStream = new FileStream(path, FileMode.Create, FileAccess.Write);
            stream.CopyTo(fileStream);
            fileStream.Dispose();
        }
        #endregion
    }
}
