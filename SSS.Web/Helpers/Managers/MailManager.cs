﻿using NLog;
using SSS.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Resources;

namespace SSS.Helpers
{
    public class MailManager
    {
        #region Initialization
        //Singleton object creation
        private static MailManager instance;

        private static Logger logger = LogManager.GetCurrentClassLogger();


        public static SmtpSettings SmtpSettings { get; set; }

        string emailsTemplatePath =  ConfigurationManager.AppSettings["EmailTemplatePath"];

        private static object syncRoot = new Object();

        private MailManager()
        {
            SmtpSettings = new SmtpSettings();
            SmtpSettings.Account = ConfigurationManager.AppSettings["Smtp.Account"];
            SmtpSettings.Password = ConfigurationManager.AppSettings["Smtp.Password"];
            SmtpSettings.Port = Convert.ToInt32(ConfigurationManager.AppSettings["Smtp.Port"]);
            SmtpSettings.Server = ConfigurationManager.AppSettings["Smtp.Server"];
            SmtpSettings.Timeout = Convert.ToInt32(ConfigurationManager.AppSettings["Smtp.Timeout"]);
            SmtpSettings.UseSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["Smtp.UseSsl"]);
        }

        public static MailManager Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                            instance = new MailManager();
                    }
                }
                return instance;
            }
        }
        #endregion

        #region Messages
        private String MessageFinalTemplate(string Title, string Content, string Image)
        {
            string imageUrl = ConfigurationManager.AppSettings["EmailImagesUrl"];
            Image = imageUrl + Image;
            logger.Info("Image Path: " + Image);

            StringBuilder oBuilder = new StringBuilder();

            // Levantar el template en memoria
            var templateFile = from line in File.ReadAllLines(Path.Combine(emailsTemplatePath, "EmailTemplate.html")) select line;
            foreach (var item in templateFile)
            { oBuilder.Append(item.Replace("[TITLE]", Title).Replace("[CONTENT]", Content).Replace("bgmail", Image)); }
            return oBuilder.ToString();
        }
        #endregion

        #region SendMail
        private void SendMailMessage(string EmailTo, string EmailSubject, string Title, string Content, string ImgBackground)
        {
            try
            {
                SendMail.SendMailMessage(EmailTo, new String[] { "" }, EmailSubject, MessageFinalTemplate(Title, Content, ImgBackground), SmtpSettings);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
        }

        public void SendMailMessageTimsheetVerification(string EmailTo, string EmailSubject, string FullName)
        {
            try
            {
                string Title = Resource.PendingTimesheet;
                string Content = Resource.Greetings + " " + FullName + ", ";
                Content = Content + "<br/><br/><br/>";
                Content = Content + Resource.TimesheetSuccessfullySentToSupervisor;
                string ImgBackground = "btpv";

                SendMail.SendMailMessage(EmailTo, new String[] { "" }, EmailSubject, MessageFinalTemplate(Title, Content, ImgBackground), SmtpSettings);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
        }

        public void SendMailMessageTimsheetWasSubmitted(string EmailTo, string EmailSubject, string FullName, string period, string supervisors)
        {
            try
            {
                string Title = Resource.TimesheetSubmitted;
                string Content = Resource.Greetings + " " + FullName + ", ";
                Content = Content + "<br/><br/><br/>";
                Content = Content + Resource.YourTimesheetForPeriod + "<strong>" + period + "</strong> " + Resource.HasBeenSuccessfullySubmittedToSupervisorOrDelegate + " <strong>" + supervisors + "</strong> ";
                string ImgBackground = "bts";

                SendMail.SendMailMessage(EmailTo, new String[] { "" }, EmailSubject, MessageFinalTemplate(Title, Content, ImgBackground), SmtpSettings);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
        }

        public void SendMailMessageTimesheetApproved(string EmailTo, string EmailSubject, string FullName, string period, string supervisorName)
        {
            try
            {
                string Title = Resource.TimesheetApproved;
                string Content = Resource.Greetings + " " + FullName + ", ";
                Content = Content + "<br/><br/><br/>";
                Content = Content + Resource.YourTimesheetForPeriod + "<strong>" + period + "</strong> " + Resource.HasBeenSuccessfullyApprovedBySupervisorOrDelegate + " <strong>" + supervisorName + "</strong> ";
                string ImgBackground = "bts";

                SendMail.SendMailMessage(EmailTo, new String[] { "" }, EmailSubject, MessageFinalTemplate(Title, Content, ImgBackground), SmtpSettings);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
        }

        public void SendMailMessageTimesheetSentToRevision(string EmailTo, string EmailSubject, string FullName, string period, string supervisorName)
        {
            try
            {
                string Title = Resource.TimesheetSentToRevision;
                string Content = Resource.Greetings + " " + FullName + ", ";
                Content = Content + "<br/><br/><br/>";
                Content = Content + Resource.YourTimesheetForPeriod + "<strong>" + period + "</strong> " + Resource.HasBeenSentToRevisionBySupervisorOrDelegate + " <strong>" + supervisorName + "</strong> ";
                string ImgBackground = "bts";

                SendMail.SendMailMessage(EmailTo, new String[] { "" }, EmailSubject, MessageFinalTemplate(Title, Content, ImgBackground), SmtpSettings);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
        }

        public void SendMailMessageTimsheetWasReceived(string EmailTo, string EmailSubject, string FullName)
        {
            try
            {
                string Title = Resource.TimesheetSuccessfullySent;
                string Content = Resource.Greetings + " " + FullName + ", ";
                Content = Content + "<br/><br/><br/>";
                Content = Content + Resource.TimesheetSuccessfullySentToSupervisor;
                string ImgBackground = "btr";

                SendMail.SendMailMessage(EmailTo, new String[] { "" }, EmailSubject, MessageFinalTemplate(Title, Content, ImgBackground), SmtpSettings);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
        }

        public void SendMailMessageTimsheetNeedRevision(string EmailTo, string EmailSubject, string FullName)
        {
            try
            {
                string Title = Resource.TimesheetNeedsRevision;
                string Content = Resource.Greetings + " " + FullName + ", ";
                Content = Content + "<br/><br/><br/>";
                Content = Content + Resource.TimesheetSuccessfullySentToSupervisor; ;
                string ImgBackground = "btnr";

                SendMail.SendMailMessage(EmailTo, new String[] { "" }, EmailSubject, MessageFinalTemplate(Title, Content, ImgBackground), SmtpSettings);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
        }

        public void SendMailMessageTimsheetSubmittedByEmployee(string EmailTo, string EmailSubject, string FullName, string EmployeeName, string period)
        {
            try
            {
                string Title = Resource.TimesheetSubmittedByEmployee;
                string Content = Resource.Greetings + " " + FullName + ", ";
                Content = Content + "<br/><br/><br/>";
                Content = Content + Resource.TimesheetSubmittedByEmployeeMess + " <strong>" + EmployeeName + "</strong>" + " " + Resource.ForPeriod + " <strong>" + period + "</strong>";
                string ImgBackground = "btnr";
                SendMail.SendMailMessage(EmailTo, new String[] { "" }, EmailSubject, MessageFinalTemplate(Title, Content, ImgBackground), SmtpSettings);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
        }

        public void SendMailMessageTimsheetPendingForCheck(string EmailTo, string EmailSubject, string FullName, List<MailListEmployees> employees)
        {
            try
            {
                string Title = Resource.TimesheetPendingForYourApprobation;
                string Content = Resource.Greetings + " " + FullName + ", ";
                Content = Content + "<br/><br/><br/>";
                Content = Content + Resource.FollowingTimesheetsArePendingForApprobation;
                Content = Content + "<br/><br/><br/>";
                Content = Content + "<table border='1' cellpadding='0' cellspacing='0' style='background-color:#FFFFFF'>";
                Content = Content + "   <thead>";
                Content = Content + "     <tr>";
                Content = Content + "      <th align='center' style='font-weight:bold'>ID</th>";
                Content = Content + "      <th align='left' style='font-weight:bold'>Employee Name</th>";
                Content = Content + "      <th align='center' style='font-weight:bold'>Period</th>";
                Content = Content + "     </tr>";
                Content = Content + "   </thead>";
                Content = Content + "   <tbody>";
                foreach (MailListEmployees e in employees)
                {
                    Content = Content + "     <tr>";
                    Content = Content + "      <td align='center'>" + e.EmployeeId + "</td>";
                    Content = Content + "      <td align='left'>" + e.FullName + "</td>";
                    Content = Content + "      <td align='center'>" + e.Period + "</td>";
                    Content = Content + "     </tr>";
                }
                Content = Content + "   </tbody>";
                Content = Content + "</table>";
                string ImgBackground = "btpc";

                SendMail.SendMailMessage(EmailTo, new String[] { "" }, EmailSubject, MessageFinalTemplate(Title, Content, ImgBackground), SmtpSettings);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
        }

        public void SendMailMessageTimsheetPendingForApprove(string EmailTo, string EmailSubject, string FullName, List<MailListEmployees> employees)
        {
            try
            {
                string Title = Resource.TimesheetPendingForYourApprobation;
                string Content = Resource.Greetings + " " + FullName + ", ";
                Content = Content + "<br/><br/><br/>";
                Content = Content + Resource.FollowingTimesheetsArePendingForApprobation;
                Content = Content + "<br/><br/><br/>";
                Content = Content + "<table border='1' cellpadding='0' cellspacing='0' style='background-color:#FFFFFF'>";
                Content = Content + "   <thead>";
                Content = Content + "     <tr>";
                Content = Content + "      <th align='center' style='font-weight:bold'>ID</th>";
                Content = Content + "      <th align='left' style='font-weight:bold'>Employee Name</th>";
                Content = Content + "      <th align='center' style='font-weight:bold'>Period</th>";
                Content = Content + "     </tr>";
                Content = Content + "   </thead>";
                Content = Content + "   <tbody>";
                foreach (MailListEmployees e in employees)
                {
                    Content = Content + "     <tr>";
                    Content = Content + "      <td align='center'>" + e.EmployeeId + "</td>";
                    Content = Content + "      <td align='left'>" + e.FullName + "</td>";
                    Content = Content + "      <td align='center'>" + e.Period + "</td>";
                    Content = Content + "     </tr>";
                }
                Content = Content + "   </tbody>";
                Content = Content + "</table>";
                string ImgBackground = "btpa";

                SendMail.SendMailMessage(EmailTo, new String[] { "" }, EmailSubject, MessageFinalTemplate(Title, Content, ImgBackground), SmtpSettings);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
        }


        #endregion
    }
}
