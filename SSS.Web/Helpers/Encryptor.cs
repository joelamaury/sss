﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace SSS.Helpers
{
    public class Encryptor
    {
        public static String GetSHA512Hash(String text)
        {
            SHA512 alg = SHA512.Create();
            byte[] result = alg.ComputeHash(System.Text.Encoding.UTF8.GetBytes(text));
            return Convert.ToBase64String(result);
        }

        /// <summary>
        /// Encriptar una cadena recibida como parametro [secretText] devolviendo su correspondiente cadena encriptada
        /// Para ello se toma caracter por caracter y se lo convierte a su valor ASCII mÃ¡s un Ã­ndice obtenido en forma random. 
        /// De esta manera una misma clave puede ser convertida en forma diferente cada vez que se encripte.
        /// </summary>
        public static String EncryptText(String secretText)
        {
            String publicText = String.Empty;
            Random numberRandom = new Random();
            Int32 index = 0;
            Int32 chr = 0;

            for (int i = 0; i < secretText.Length; i++)
            {
                index = numberRandom.Next(0, 99);
                chr = Encoding.UTF8.GetBytes(secretText.Substring(i, 1)).FirstOrDefault() + index;
                publicText += String.Format("a{0}i{1}", chr, index);
            }

            return String.IsNullOrEmpty(publicText) ? String.Empty : String.Format("{0}a", publicText);
        }

        /// <summary>
        /// Desencriptar una cadena recibida como parametro [publicText] devolviendo su correspondiente cadena privada (descifrada)
        /// Para ello se recorre la cadena recibida y se obtiene de allÃ­ cada caracter representado en codigo ASCII con un incremento dado por el
        /// Ã­ndice que lo secunda. Se obtienen estos dos valores y se restan para obtener el valor ASCII original de la letra convirtiendolo a cadena
        /// </summary>
        public static String Unencrypt(String publicText)
        {
            String publicTextTmp = publicText;
            String secretText = String.Empty;
            Int32 index = 0;
            Int32 chr = 0;

            try
            {
                foreach (var part in publicTextTmp.Split('a'))
                {
                    if (!String.IsNullOrEmpty(part))
                    {
                        chr = Convert.ToInt32(part.Split('i')[0]);
                        index = Convert.ToInt32(part.Split('i')[1]);
                        secretText += (char)(chr - index);
                    }
                }
            }
            catch (Exception)
            {
                secretText = publicText;
            }

            return secretText;
        }
    }
}