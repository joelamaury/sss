﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SSS.Models
{
    public class AllowedCompanyModel
    {
        public int LaborLevelEntryId { get; set; }
        public string Name { get; set; }
        public bool Allowed { get; set; }
    }
}