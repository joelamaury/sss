﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SSS.Models
{
    public class PayCodeConfigurationModel
    {
        public int PayCodeId { get; set; }
        public string PayCodeName { get; set; }
        public bool Show { get; set; }
        public bool Regular { get; set; }
        public bool Overtime { get; set; }
        public bool SenToSap { get; set; }

        public bool IsShow { get; set; }
        public bool IsRegular { get; set; }
        public bool IsOvertime { get; set; }
        public bool IsSendToSap{ get; set; }
    }
}