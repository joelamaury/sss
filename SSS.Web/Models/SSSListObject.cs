﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SSS.Models
{
    public class SSSListObject
    {
        public string Value { get; set; }
        public string Description { get; set; }
    }
}