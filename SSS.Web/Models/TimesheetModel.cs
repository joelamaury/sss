﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SSS.Models
{
    public class TimesheetModel
    {
        public int EmployeeId { get; set; }
        public int EmployeeIdEditingTimesheet { get; set; }
        public string TimseheetDate { get; set; }
        public List<SSSListObject> ConceptList { get; set; }
        public List<SSSListObject> CostCenterList { get; set; }
        public List<SSSListObject> InternalOrderList { get; set; }

        public TimesheetModel()
        {
            ConceptList = new List<SSSListObject>();
            CostCenterList = new List<SSSListObject>();
            InternalOrderList = new List<SSSListObject>();
        }
    }
}