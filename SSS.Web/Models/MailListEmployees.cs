﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SSS.Models
{
    public class MailListEmployees
    {
        public string EmployeeId { get; set; }
        public string FullName { get; set; }
        public string Period { get; set; }

    }
}