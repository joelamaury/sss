﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SSS.Models
{
    public class PayRuleConfigurationModel
    {
        public int PayRuleId { get; set; }
        public string PayRuleName { get; set; }
        public bool Checked { get; set; }
    }
}