﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SSS.Models
{
    public class SmtpSettings
    {
        public string Server { get; set; }
        public string Account { get; set; }
        public string Password { get; set; }
        public int Port { get; set; }
        public bool UseSsl { get; set; }
        public int Timeout { get; set; }

    }
}