﻿
function GetValidationMessage(controlId,message)
{
    return '<span style="margin-top:20px" class="k-widget k-tooltip k-tooltip-validation k-invalid-msg field-validation-error" data-for="' + controlId + '" data-valmsg-for="' + controlId + '" id="' + controlId + '_validationMessage" role="alert"><span class="k-icon k-i-warning"> </span>' + message + '</span>';
}

function GetFullAddress(line1,line2,cityId,stateId,countryId,zipCode)
{
    var address = '';
    address += line1;
    address += (line2 != null && line2 != '') ? (" " + line2) : "";
    address += $('#' + cityId).val() != '' ? (" " + $('#'+cityId).val()) : "";
    address += $('#' + stateId).data('kendoDropDownList') && $('#' + stateId).data('kendoDropDownList').value() != '' ? (" " + $('#'+stateId).data('kendoDropDownList').text()) : "";
    address += $('#' + countryId).data('kendoDropDownList')&&$('#' +countryId).data('kendoDropDownList').value() != '' ? (" " + $('#'+countryId).data('kendoDropDownList').text()) : "";
    address += (zipCode != '' && zipCode != null) ? (" " + zipCode.replace(/_/g, "")) : "";

    return address;
}

function GetFullName(name, initial, lastName)
{
    var fullNm = '';
    fullNm += name;
    fullNm += (initial != null + initial != '') ? (" " + initial) : "";
    fullNm += (lastName != null && lastName != '') ? (" " + lastName) : "";

    return fullNm;
}

function ShowModal(controlId)
{
    $('#'+controlId).modal({ backdrop: 'static', keyboard: false });
}

function HideModal(controlId)
{
    $('#' + controlId).modal('hide');
}

function NotificationMessage(strongMess,mess,type)    
{
    $("#notificationMessage").removeClass('alert-success');
    $("#notificationMessage").removeClass('alert-info');
    $("#notificationMessage").removeClass('alert-warning');
    $("#notificationMessage").removeClass('alert-danger');

    $("#notificationMessage #strongMessage").text(strongMess);
    $("#notificationMessage #mess").text(mess);

    switch (type)
    {
        case 's':
            {
                $("#notificationMessage").addClass('alert-success');
                $('#imgNotificationImage').attr('src', '../../Content/Images/checked.png');
                break;
            }
        case 'w':
            {
                $("#notificationMessage").addClass('alert-warning');
                $('#imgNotificationImage').attr('src', '../../Content/Images/warning.png');
                break;
            }
        case 'd':
            {
                $("#notificationMessage").addClass('alert-danger');
                $('#imgNotificationImage').attr('src', '../../Content/Images/forbidden.png');
                break;
            }
        default :
            {
                $("#notificationMessage").addClass('alert-info');
                $('#imgNotificationImage').attr('src', '../../Content/Images/warning.png');
                break;
            }
    }

    $("#notificationMessage").fadeIn(600, function ()
    {
        $("#notificationMessage").fadeOut(5500);
    });
}

function DisableInputControls(sectionId)
{
    $('#' + sectionId + ' input').prop('disabled', 'disabled');
    $('#' + sectionId + ' textarea').prop('disabled', 'disabled');

    $('#' + sectionId + ' input').each(function ()
    {
        var dropDownList = $(this).data("kendoDropDownList");
        if (dropDownList)
        {
            dropDownList.enable(false);
        }

        var datePicker = $(this).data("kendoDatePicker");
        if (datePicker)
        {
            datePicker.enable(false);
        }
    });

    $('#' + sectionId + ' select').each(function ()
    {
        var multiSelect = $(this).data("kendoMultiSelect");
        if (multiSelect)
        {
            multiSelect.enable(false);
        }
    });

    $('#' + sectionId + ' .k-grid .k-grid-add').removeClass("k-grid-add").addClass("k-state-disabled").removeAttr("href");
}

function cleanPhone(phone)
{
    if (phone != null && phone != "")
        phone = phone.replace("(", "").replace(")", "").replace("-", "").replace(" ", "").replace("_", "");
    return phone;
}

function popUpError_handler(e)
{
    if (e.errors)
    {
        var grid = $('#grid').data("kendoGrid");
        grid.one("dataBinding", function (ev)
        {
            ev.preventDefault();
            var message = "Errors:\n";
            $.each(e.errors, function (key, value)
            {
                if ('errors' in value)
                {
                    $.each(value.errors, function ()
                    {
                        $(".formErrorMsgs").append('<label class="error" id="user-error" for="user">' + this + '</label>');
                    });
                }
            });
        });
    }
}

function ResizePopWindowDialog(windowName)
{
    var window = $("#" + windowName);
    if (window != null)
    {
        var h = window.height();
        var headH = $("#" + windowName + " .k-header").outerHeight(true);
        var footH = $("#" + windowName + " .popUpWindowButtons").outerHeight(true);
        var contH = h - footH - headH - 60;
        if (windowName == "windowChangeEmployeeSelection")
        {
            if ($("#windowChangeEmployeeSelection").find("#currentHyperfindInfo").css('display') != 'none')
            {
                contH = contH - 30;
                window.parent().css("left", "5%");
            }
        }
        $("#" + windowName + " .windowPopUpContent").height(contH).css("overflow", "auto");
    }
}
