﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Configuration;
using Resources;
using NLog;
using System.Threading;
using System.Globalization;
using SSS.Helpers;
using ITSV6.Model;
using API_Provider.Core;
using Models;
using SSS.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using Models.Core;

namespace SSS.Controllers
{
    public class ApprovalsController : BaseController
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult SearchEmployees([DataSourceRequest] DataSourceRequest request, string name, string id, DateTime? fromDate, DateTime? toDate, List<string> statusList)
        {
            int loggedUserId = 0;
            if (Session["user"] != null)
            {
                USERACCOUNT userAcc = (USERACCOUNT)Session["user"];
                if (userAcc != null)
                {
                    loggedUserId = userAcc.PERSONID;
                }
            }


            List<EmployeeInfoModel> list = new EmployeeInfoModel().SearchEmployees(loggedUserId, name, id, fromDate, toDate, statusList);
            return Json(list.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetEmployeePeriods([DataSourceRequest] DataSourceRequest request, int personId)
        {
            TimesheetPeriodModel model = new TimesheetPeriodModel();

            return Json(model.GetTimesheetPeriod(personId).ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

    }
}