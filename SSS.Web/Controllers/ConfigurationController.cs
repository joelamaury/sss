﻿using SSS.Models;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Models;
using Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Resources;
using System.Web;
using System.Web.Mvc;

namespace SSS.Controllers
{
    public class ConfigurationController : BaseController
    {
        #region Profile Permissions

        public ActionResult Permissions()
        {
            var kronosContext = GetKronosContext();
            var sssContext = GetSSSContext();


            List<AllowedCompanyModel> kronosEntries = kronosContext.LABORLEVELENTRies.Where(l=>l.LABORLEVELDEFID == 1).Select(l => new AllowedCompanyModel { LaborLevelEntryId = l.LABORLEVELENTRYID, Name = l.DESCRIPTION }).Take(10).ToList();
            List<CompanyPermission> companyPermissions = sssContext.CompanyPermissions.ToList();
            CompanyPermission permission;
            foreach (AllowedCompanyModel item in kronosEntries)
            {
                permission = companyPermissions.Where(p => p.LaborLevelEntryId == item.LaborLevelEntryId).FirstOrDefault();
                if (permission!= null)
                {
                    item.Allowed = permission.Allowed;
                }
            }

            ViewData["CompanyList"] = kronosEntries;
            ViewData["ProfileList"] = kronosContext.LOGONPROFILEs.Select(m => new SSSListObject { Description = m.PROFILENM, Value = m.LOGONPROFILEID.ToString() }).OrderBy(p => p.Description).ToList();
            return View();
        }

        public ActionResult _Sections(int profileId)
        {
            var context = GetSSSContext();

            List<AppSection> secList = context.AppSections.OrderBy(s => s.Name).ToList();
            List<ProfilePermission> grantedList = context.ProfilePermissions.Where(m => m.LogonProfileId == profileId).ToList();
            List<PermissionModel> list = new List<PermissionModel>();
            int notGrantedId = 0;
            foreach (AppSection child in secList)
            {
                PermissionModel model = new PermissionModel { Name = child.Name, ProfileId = profileId, SectionId = child.Id };

                ProfilePermission granted = grantedList.Where(p => p.AppSectionId == child.Id && p.LogonProfileId == profileId).FirstOrDefault();
                if (granted != null)
                {
                    model.Id = granted.Id;
                    model.Checked = true;
                }
                else
                {
                    model.Id = --notGrantedId;
                    model.Checked = false;
                }

                list.Add(model);
            }

            return View(list);
        }

        public JsonResult SavePermissions(List<PermissionModel> permissions, List<int> companies)
        {
            var context = GetSSSContext();

            if (permissions != null)
            {
                foreach (PermissionModel child in permissions)
                {
                    if (child.Id > 0 && child.Checked == false)
                    {
                        ProfilePermission perm = context.ProfilePermissions.Where(p => p.Id == child.Id).FirstOrDefault();
                        context.ProfilePermissions.Remove(perm);
                    }
                    else
                    {
                        if (child.Checked)
                        {
                            context.ProfilePermissions.Add(new ProfilePermission { AppSectionId = child.SectionId, LogonProfileId = child.ProfileId });
                        }
                    }
                }
            }

            context.CompanyPermissions.RemoveRange(context.CompanyPermissions.ToList());
            if(companies !=null)
            {
                foreach (int item in companies)
                {
                    context.CompanyPermissions.Add(new CompanyPermission { Allowed = true, LaborLevelEntryId = item });
                }
                
            }

            context.SaveChanges();

            return Json(new { status = "success" });
        }

        #endregion

        #region Pay Codes

        public ActionResult PayCodes()
        {          
            return View();
        }

        public ActionResult _PayCodes(int payCodeAction)
        {
            var kronosContext = GetKronosContext();
            var sssContext = GetSSSContext();

            List<PayCodeConfiguration> sssPayCodeList = sssContext.PayCodeConfigurations.ToList();

            List<PayCodeConfigurationModel> list = kronosContext.PAYCODEs.Where(s => s.VISIBLE_TO_USER.HasValue && s.VISIBLE_TO_USER.Value == 1).OrderBy(s => s.NAME).Select(p => new PayCodeConfigurationModel { PayCodeId = p.PAYCODEID, PayCodeName = p.NAME }).ToList();

            PayCodeConfiguration sssPc = null;
            foreach (PayCodeConfigurationModel item in list)
            {
                sssPc = sssPayCodeList.Where(s => s.KronosId == item.PayCodeId).FirstOrDefault();

                if (sssPc != null)
                {
                    item.IsOvertime = sssPc.Overtime;
                    item.IsShow = sssPc.Show;
                    item.IsRegular = sssPc.Regular;
                    item.IsSendToSap = sssPc.SendToSap;

                    switch (payCodeAction)
                    {
                        case 0:
                            {
                                item.Show = sssPc.Show;
                                break;
                            }
                        case 1:
                            {
                                item.Show = sssPc.Regular;
                                break;
                            }
                        case 2:
                            {
                                item.Show = sssPc.Overtime;
                                break;
                            }
                        case 3:
                            {
                                item.Show = sssPc.RegularDefault;
                                break;
                            }
                        case 4:
                            {
                                item.Show = sssPc.SendToSap;
                                break;
                            }
                    }
                }
            }

            bool showRadio = false;
            if(payCodeAction == 3)
            {
                showRadio = true;
            }

            ViewData["ShowRadio"] = showRadio;

            if(payCodeAction == 4)
            {
                list = list.Where(l => l.IsShow == true || l.IsRegular == true || l.IsOvertime == true).ToList();
            }

            return View(list);
        }

        public JsonResult SavePayCodes(List<int> checkedList, List<int> uncheckedList, int payCodeAction)
        {
            if (checkedList != null)
            {
                foreach (int item in checkedList)
                {
                    ManageSssPayCode(item, true, payCodeAction);
                }
            }

            if (uncheckedList != null)
            {
                foreach (int item in uncheckedList)
                {
                    ManageSssPayCode(item, false, payCodeAction);
                }
            }

            return Json(new { status = "success" });
        }

        private void ManageSssPayCode(int item, bool chk, int payCodeAction)
        {
            var context = GetSSSContext();

            PayCodeConfiguration sssPc = null;
            sssPc = context.PayCodeConfigurations.Where(p => p.KronosId == item).FirstOrDefault();

            if (sssPc == null)
            {
                sssPc = new PayCodeConfiguration { KronosId = item };
                SetCorrespondingProppertybasedOnAction(sssPc, payCodeAction, chk);
                context.PayCodeConfigurations.Add(sssPc);
            }
            else
            {
                SetCorrespondingProppertybasedOnAction(sssPc, payCodeAction, chk);
            }

            context.SaveChanges();

        }

        private void SetCorrespondingProppertybasedOnAction(PayCodeConfiguration sssPc, int payCodeAction, bool chk)
        {
            switch (payCodeAction)
            {
                case 0:
                    {
                        sssPc.Show = chk;
                        break;
                    }
                case 1:
                    {
                        sssPc.Regular = chk;
                        break;
                    }
                case 2:
                    {
                        sssPc.Overtime = chk;
                        break;
                    }
                case 3:
                    {
                        sssPc.RegularDefault = chk;
                        break;
                    }
                case 4:
                    {
                        sssPc.SendToSap = chk;
                        break;
                    }
            }
        }
        #endregion

        #region PayRuleConfiguration

        public ActionResult PayRules()
        {
            var sssContext = GetSSSContext();
            var kronosContext = GetKronosContext();

            List<PAYRULEID> kronosPayRuleList = kronosContext.PAYRULEIDS.ToList();
            List<PayRuleConfiguration> ssssPayRuleList = sssContext.PayRuleConfigurations.ToList();

            List<PayRuleConfigurationModel> list = new List<PayRuleConfigurationModel>();

            foreach (PAYRULEID item in kronosPayRuleList)
            {
                list.Add(new PayRuleConfigurationModel { PayRuleId = item.PAYRULEID1, PayRuleName = item.NAME, Checked = ssssPayRuleList.Where(p => p.PayRuleId == item.PAYRULEID1).Count() > 0 ? true : false });
            }

            return View(list);
        }

        public JsonResult SavePayRules(List<int> checkedList)
        {
            var sssContext = GetSSSContext();

            sssContext.PayRuleConfigurations.RemoveRange(sssContext.PayRuleConfigurations.ToList());
            sssContext.SaveChanges();

            foreach (int item in checkedList)
            {
                sssContext.PayRuleConfigurations.Add(new PayRuleConfiguration { PayRuleId = item });
            }

            sssContext.SaveChanges();

            return Json(new { status = "success" });
        }

        #endregion
    }
}
