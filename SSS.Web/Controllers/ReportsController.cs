﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Configuration;
using Resources;
using NLog;
using System.Threading;
using System.Globalization;
using SSS.Helpers;
using ITSV6.Model;
using API_Provider.Core;
using Models;
using SSS.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using Models.Core;

namespace SSS.Controllers
{
    public class ReportsController : BaseController
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        } 

    }
}