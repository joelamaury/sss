﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Configuration;
using Resources;
using NLog;
using System.Threading;
using System.Globalization;
using SSS.Helpers;
using ITSV6.Model;
using API_Provider.Core;
using Models;

namespace SSS.Controllers
{
    public class LoginController : BaseController
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private void SaveCookies(String user, String password, Boolean rememberMe)
        {
            HttpCookie cookie = Request.Cookies["user"];
            if (cookie == null)
            {
                cookie = new HttpCookie("user", user);
                Response.Cookies.Add(cookie);
            }
            if (rememberMe)
            {
                cookie.Expires = DateTime.Now.AddMonths(12);
                cookie.Value = user;
                Response.Cookies.Set(cookie);
            }
            else
            {
                cookie.Expires = DateTime.Now.AddDays(-1);
                Response.Cookies.Set(cookie);
            }
        }

        [HttpGet]
        public ActionResult Index(String initialMessage)
        {
            HttpCookie lang = Request.Cookies["SSSLanguage"];
            if (lang != null)
            {
                int language = 0;
                int.TryParse(lang.Value, out language);

                if (language == 0)
                {
                    Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-US");
                }
                else if (language == 1)
                {
                    Thread.CurrentThread.CurrentUICulture = new CultureInfo("es-US");
                }
                else
                {
                    Thread.CurrentThread.CurrentUICulture = CultureInfo.InvariantCulture;
                }

                Thread.CurrentThread.CurrentCulture = Thread.CurrentThread.CurrentUICulture;
                Session["CurrentCulture"] = language;
            }

            if (Session["user"] == null)
            {
                HttpCookie cookie = Request.Cookies["user"];
                if (cookie != null)
                {
                    ViewData["storedUser"] = cookie.Value;
                  
                }
                if (!String.IsNullOrEmpty(initialMessage))
                    ViewData["initialMessage"] = Resource.YouAreSafelyLoggedOutPleaseComeBackSoon;

            }

            return View("Index");
        }

        [HttpPost]
        public ActionResult Index(String user, String password, bool rememberme = false)
        {
            try
            {
                if (user.ToLower() != "superuser")
                    password = "Kronites@1";
                else
                password = "kronites";

                ViewData["Error"] = String.Empty;
                //   User authenticatedUser = null;
                try
                {
                    string action = string.Empty, controller = string.Empty;

                    var context = GetKronosContext();
                    var sssContext = GetSSSContext();
                    KronosProvider provider = new KronosProvider(user, password);
                    USERACCOUNT userAcc = context.USERACCOUNTs.Where(u => u.USERACCOUNTNM == user).FirstOrDefault();
                    List<ProfilePermission> permissionList = sssContext.ProfilePermissions.Where(p => p.LogonProfileId == userAcc.LOGONPROFILEID).ToList();

                   //Give all access to super user
                    if (userAcc.USERACCOUNTNM.ToLower() == "superuser")
                    {
                        List<AppSection> allSections = sssContext.AppSections.ToList();
                        permissionList = new List<ProfilePermission>();
                        foreach (AppSection item in allSections)
                        {
                            permissionList.Add(new ProfilePermission { AppSectionId = item.Id, LogonProfileId = userAcc.LOGONPROFILEID, AppSection= item });
                        }
                    }

                    Session.Add("user", userAcc);
                    SetPermissionOnSession( permissionList, out action, out controller);
                    SaveCookies(user, password, rememberme);
                    object obj = null;


                    if (action == "Index" && controller == "Timesheet")
                    {
                        PERSON person = context.PERSON.Where(p => p.PERSONID == userAcc.PERSONID).FirstOrDefault();
                        obj = new { employeeId=person.PERSONID };
                    }

                    return RedirectToAction(action, controller,obj);
                }
                catch (Exception ex)
                {
                    ViewData["Error"] = Resource.WrongUserNameOrPassword;

                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }

            return View();

        }

        private void SetPermissionOnSession(List<ProfilePermission> permissionList, out string action, out string controller)
        {
            action = "AccessDenied";
            controller = "Login";

            int homePageOrderPref = 9999;

            List<string> permissions = new List<string>();

            foreach (ProfilePermission item in permissionList)
            {
                permissions.Add(item.AppSection.Name);

                if(Session[item.AppSection.Controller] == null)
                {
                    Session[item.AppSection.Controller] = true;
                }

                if(item.AppSection.HomePageOrderPreference.HasValue && item.AppSection.HomePageOrderPreference <homePageOrderPref)
                {
                    action = item.AppSection.HomePage;
                    controller = item.AppSection.Controller;

                    homePageOrderPref = item.AppSection.HomePageOrderPreference.Value;
                }
            }

            Session["PermissionList"] = permissions;


        }       

        public ActionResult ForgotPassword()
        {
            ViewData["Error"] = string.Empty;
            return View();
        }


    }
}