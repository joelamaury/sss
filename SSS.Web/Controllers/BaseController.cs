﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Threading;
using System.Globalization;
using SSS.Helpers;
using NLog;
using Models;
using SSS.Models;
using Resources;
using Models.Core;

namespace SSS.Controllers
{
    public class BaseController : Controller
    {
        public static Logger logger = LogManager.GetCurrentClassLogger();

        public KronosEntities GetKronosContext()
        {
            return new KronosEntities();
        }

        public SSSEntities GetSSSContext()
        {
            return new SSSEntities();
        }

        public int GetLoggedUserId()
        {
            int loggedUserId = 0;
            if (Session["user"] != null)
            {
                USERACCOUNT userAcc = (USERACCOUNT)Session["user"];
                if (userAcc != null)
                {
                    loggedUserId = userAcc.PERSONID;
                }
            }
            return loggedUserId;
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            int culture = 0;
            if (this.Session == null || this.Session["CurrentCulture"] == null)
            {
                if (Request.Cookies["SSSLanguage"] == null)
                {
                    HttpCookie langCookie = new HttpCookie("SSSLanguage");
                    langCookie.Value = culture.ToString();
                    langCookie.Expires = DateTime.Today.AddYears(10);
                    Response.Cookies.Add(langCookie);
                }
                else
                {
                    int.TryParse(Request.Cookies["SSSLanguage"].Value, out culture);
                }
                this.Session["CurrentCulture"] = culture;
            }
            else
            {
                culture = (int)this.Session["CurrentCulture"];
            }
            //Cultura actual.VF.    
            CultureHelper.CurrentCulture = culture;

            HttpSessionStateBase session = filterContext.HttpContext.Session;
            // If the browser session or authentication session has expired...
            if (session.IsNewSession || Session["user"] == null)
            {
                if (filterContext.HttpContext.Request.IsAjaxRequest())
                {
                    // For AJAX requests, return result as a simple string, 
                    // and inform calling JavaScript code that a user should be redirected.
                    JsonResult result = Json("SessionTimeout", "text/html");
                    filterContext.Result = result;
                }
                else
                {
                    if (!((filterContext.ActionDescriptor.ActionName == "Index") && filterContext.Controller.GetType().ToString() == "SSS.Controllers.LoginController"))
                    {
                        // For round-trip requests,
                        filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary { { "Controller", "Login" }, { "Action", "Index" } });
                    }
                }
            }
            else
            {
                if (filterContext.ActionDescriptor.ActionName != "AccessDenied")
                {

                    bool hasPermission = false;

                    if (filterContext.Controller.GetType().ToString() == "SSS.Controllers.LoginController")
                    {
                        hasPermission = true;
                    }

                    //Verify if user has permission to page (controller)
                    string sessionName = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName.Replace("Controller", "");
                    if (session[sessionName] != null && ((bool)Session[sessionName]))
                    {
                        ///Verify if user accessing timesheet is the owner of the timesheet or a supervisor with access to employee
                        if (filterContext.ActionDescriptor.ActionName == "Index" && sessionName == "Timesheet")
                        {
                            int employeeId = 0;
                            string empId = string.Empty;

                            empId = filterContext.ActionParameters["employeeId"] != null ? filterContext.ActionParameters["employeeId"].ToString() : string.Empty;

                            int.TryParse(empId, out employeeId);

                            if (UserHasAccessToEmployee(employeeId) || GetLoggedUserId() == employeeId)
                            {
                                hasPermission = true;
                            }
                            else
                            {
                                hasPermission = false;
                            }

                        }
                        else
                        {
                            hasPermission = true;
                        }
                    }

                    if (hasPermission == false)
                    {
                        filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary { { "Controller", "Login" }, { "Action", "AccessDenied" } });
                    }
                }

            }
        }

        public ActionResult Logout()
        {
            Session.RemoveAll();
            return RedirectToAction("Index", "Login");
        }

        public JsonResult ChangeCulture(int language)
        {
            if (language == 0)
            {
                Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-US");
            }
            else if (language == 1)
            {
                Thread.CurrentThread.CurrentUICulture = new CultureInfo("es-US");
            }
            else
            {
                Thread.CurrentThread.CurrentUICulture = CultureInfo.InvariantCulture;
            }

            Thread.CurrentThread.CurrentCulture = Thread.CurrentThread.CurrentUICulture;
            Session["CurrentCulture"] = language;

            if (Request.Cookies["SSSLanguage"] == null)
            {
                HttpCookie langCookie = new HttpCookie("SSSLanguage");
                langCookie.Value = language.ToString();
                langCookie.Expires = DateTime.Today.AddYears(10);
                Response.Cookies.Add(langCookie);
            }
            else
            {
                HttpCookie langCookie = Request.Cookies["SSSLanguage"];
                langCookie.Value = language.ToString();
                langCookie.Expires = DateTime.Today.AddYears(10);
                Response.Cookies.Set(langCookie);
            }

            return Json(new { status = "success" });
        }

        public bool UserHasAccessToEmployee(int employeeId)
        {
            int loggedUserId = GetLoggedUserId();

            List<int> employeesForSypervisor = new EmployeeInfoModel().GetEmployeeIdsForSupervisor(loggedUserId);

            return employeesForSypervisor.Contains(employeeId);
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            Exception e = filterContext.Exception;
            logger.Error(e);
        }

        public ActionResult _EmployeeInfo(int employeeId)
        {
            EmployeeInfoModel model = new EmployeeInfoModel(employeeId);
            return View(model);
        }

        public ActionResult _TimesheetPeriodLog(int periodId)
        {
            TimesheetPeriodLogModel model = new TimesheetPeriodLogModel();
            List<TimesheetPeriodLogModel> list = model.GetTimesheetPeriodLogList(periodId);
            return View(list);
        }

        public ActionResult AccessDenied()
        {
            return View();
        }
    }
}