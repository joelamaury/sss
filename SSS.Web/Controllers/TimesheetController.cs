﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Configuration;
using Resources;
using NLog;
using System.Threading;
using System.Globalization;
using SSS.Helpers;
using ITSV6.Model;
using API_Provider.Core;
using Models;
using SSS.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using Models.Core;
using System.Threading.Tasks;

namespace SSS.Controllers
{
    public class TimesheetController : BaseController
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        [HttpGet]
        public ActionResult Index(int employeeId, DateTime? timesheetDate)
        {
            int loggedUserId = GetLoggedUserId();
            ViewData["EmployeeId"] = employeeId;

            bool isSupervisor = false;

            if(loggedUserId != employeeId)
            {
                isSupervisor = true;
            }

            ViewData["IsSupervisor"] = isSupervisor;
            ViewData["TimesheetDate"] = timesheetDate;

            return View();
        }

        public ActionResult _KronosTimesheet()
        {
            return PartialView();
        }

        public JsonResult ReadKronosTimesheet([DataSourceRequest] DataSourceRequest request, int employeeId, DateTime timesheetDate, bool? overwrite)
        {
            int loggedUserId = GetLoggedUserId();
            TimesheetGridModel model = new TimesheetGridModel();
            List<TimesheetGridModel> list = model.GetKronosTimesheetData(employeeId, timesheetDate);

            if (GetLoggedUserId() == employeeId)
            {
                model.AutoDistributeKronosTimesheet(list, employeeId, timesheetDate, loggedUserId,overwrite);
            }

            return Json(list.ToDataSourceResult(request));
        }


        public ActionResult _Timesheet()
        {
            var kronosContext = GetKronosContext();
            var sssContext = GetSSSContext();
            TimesheetModel model = new TimesheetModel();

            model.ConceptList.AddRange(kronosContext.PAYCODEs.Where(p => p.VISIBLE_TO_USER.HasValue && p.VISIBLE_TO_USER.Value == 1).Select(p => new SSSListObject { Description = p.NAME, Value = p.PAYCODEID.ToString() }).OrderBy(c => c.Description).ToList());
            model.CostCenterList.AddRange(sssContext.CostCenters.Select(p => new SSSListObject { Description = p.Name + " (" + p.Code + ")", Value = p.Id.ToString() }).OrderBy(d => d.Description).ToList());
            model.InternalOrderList.AddRange(sssContext.InternalOrders.Select(p => new SSSListObject { Description = p.Name + " (" + p.Code + ")", Value = p.Id.ToString() }).OrderBy(d => d.Description).ToList());

            return PartialView(model);
        }

        public JsonResult ReadTimesheet([DataSourceRequest] DataSourceRequest request, int employeeId, DateTime timesheetDate)
        {
            TimesheetGridModel model = new TimesheetGridModel();
            List<TimesheetGridModel> list = model.GetDistributionTimesheetData(employeeId, timesheetDate);
            return Json(list.ToDataSourceResult(request));
        }

        private void GetPeriodDates(DateTime timesheetDate, out DateTime startDate, out DateTime endDate)
        {
            startDate = timesheetDate;
            if (startDate.DayOfWeek != DayOfWeek.Sunday)
            {
                do
                {
                    startDate = startDate.AddDays(-1);
                }
                while (startDate.DayOfWeek != DayOfWeek.Sunday);
            }

            endDate = startDate.AddDays(6);
        }

        public JsonResult GetInternalOrders(int costCenterId, string text)
        {
            string companyCode = string.Empty;
            var sssContext = GetSSSContext();
            CostCenter costCenter = sssContext.CostCenters.Where(c => c.Id == costCenterId).FirstOrDefault();

            if (costCenter != null)
            {
                companyCode = costCenter.CompanyCode;
            }

            return Json(sssContext.InternalOrders.Where(x => x.CompanyCode == companyCode).Select(p => new SSSListObject { Description = p.Name + " (" + p.Code + ")", Value = p.Id.ToString() }).Where(d=>d.Description.Contains(text)), JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [HttpPost, ValidateInput(false)]
        public ActionResult CreateTimesheetDistribution([DataSourceRequest] DataSourceRequest request, TimesheetGridModel item, int employeeId, DateTime timesheetDate)
        {
            int loggedUserId = GetLoggedUserId();
            ValidateDistributionTimesheet(item, employeeId, timesheetDate, loggedUserId);

            if (ModelState.IsValid)
            {
                TimesheetDistributionModel model = new TimesheetDistributionModel();
                model.CreateTimesheetDistribution(item, employeeId, timesheetDate, loggedUserId);
            }

            return Json(new[] { item }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [HttpPost, ValidateInput(false)]
        public ActionResult UpdateTimesheetDistribution([DataSourceRequest] DataSourceRequest request, TimesheetGridModel item, int employeeId, DateTime timesheetDate)
        {
            int loggedUserId = GetLoggedUserId();
            ValidateDistributionTimesheet(item, employeeId, timesheetDate, loggedUserId);

            if (ModelState.IsValid)
            {
                TimesheetDistributionModel model = new TimesheetDistributionModel();
                model.UpdateTimesheetDistribution(item, employeeId, timesheetDate, loggedUserId);

            }

            return Json(new[] { item }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [HttpPost, ValidateInput(false)]
        public ActionResult DestroyTimesheetDistribution([DataSourceRequest] DataSourceRequest request, TimesheetGridModel item, int employeeId, DateTime timesheetDate)
        {
            try
            {
                int loggedUserId = GetLoggedUserId();
                List<int> employeesForSupervisor = new EmployeeInfoModel().GetEmployeeIdsForSupervisor(loggedUserId);
                TimesheetPeriod period = new TimesheetPeriodModel().GetTimesheetPeriod(employeeId, timesheetDate);

                if (period.Status != null && period.Status.ToLower() != "pendingsubmission" && period.Status.ToLower() != "revision" && period.Status.ToLower() != "submitted")
                {
                    ModelState.AddModelError("ConceptId", Resource.OnlyPendingToSubmitRevisionAndSubmitedCanBeEdited);
                }
                else
                {
                    if ((period.Status != null && period.Status.ToLower() == "pendingsubmission" || period.Status.ToLower() == "revision") && employeeId != loggedUserId)
                    {
                        ModelState.AddModelError("ConceptId",  Resource.OnlyEmployeeCanEditPendingToSubmitOrRevisionTimesheet );
                    }

                    if (period.Status != null && period.Status.ToLower() == "submitted" && !employeesForSupervisor.Contains(employeeId))
                    {
                    ModelState.AddModelError("ConceptId", Resource.OnlySupervisorOrDelegateCanEditSubmittedTimesheet );
                    }
                }

                if (ModelState.IsValid)
                {
                    TimesheetDistributionModel model = new TimesheetDistributionModel();
                    model.DeleteTimesheetDistribution(item);
                }
            }
            catch (Exception e)
            {
                logger.Error(e);
                ModelState.AddModelError("destroy", "Error");
            }
            return Json(new[] { item }.ToDataSourceResult(request, ModelState));
        }

        private void ValidateDistributionTimesheet(TimesheetGridModel item, int employeeId, DateTime timesheetDate, int loggedUserId)
        {
            TimesheetGridValidationModel validation = new TimesheetGridValidationModel(employeeId, timesheetDate, item);
            validation.ValidateEntry(item, loggedUserId);

            foreach (TimesheetGridValidationModel.ErrorInfo error in validation.ErrorList)
            {
                ModelState.AddModelError(error.FieldName, error.ErrorMessage);
            }
        }

        private TimesheetGridValidationModel ValidateDistributionTimesheetPeriod(int employeeId, DateTime timesheetDate, string action, int loggedUserId)
        {
            TimesheetGridValidationModel validation = new TimesheetGridValidationModel(employeeId, timesheetDate, null);
            validation.ValidatePeriod(action, loggedUserId);
            return validation;
        }

        public JsonResult TimesheetAction(string tsAction, int employeeId, DateTime timesheetDate)
        {
            int loggedUserId = GetLoggedUserId();
            switch (tsAction)
            {
                case "submit":
                    {
                        TimesheetGridValidationModel validation = ValidateDistributionTimesheetPeriod(employeeId, timesheetDate, tsAction, loggedUserId);
                        if (string.IsNullOrEmpty(validation.ValidationSummary))
                        {
                            TimesheetPeriodModel periodModel = new TimesheetPeriodModel();
                           TimesheetPeriod period = periodModel.Update(employeeId, timesheetDate, loggedUserId,"submitted");
                            string periodText = string.Empty;
                            if(period!=null)
                            {
                                periodText = period.FromDate.ToShortDateString() + " - " + period.ToDate.ToShortDateString();
                            }

                            Task.Factory.StartNew(() => SendSubmitEmails(employeeId, periodText)).ConfigureAwait(false);                           

                            return Json(new { status = "success", mess= Resource.TimesheetSuccessfullySubmitted });
                        }
                        else
                        {
                            return Json(new { status = "invalid", validationSummary = validation.ValidationSummary });
                        }
                    }
                case "approve":
                    {
                        TimesheetGridValidationModel validation = ValidateDistributionTimesheetPeriod(employeeId, timesheetDate, tsAction, loggedUserId);
                        if (string.IsNullOrEmpty(validation.ValidationSummary))
                        {
                            TimesheetPeriodModel periodMOdel = new TimesheetPeriodModel();
                            TimesheetPeriod period = periodMOdel.Update(employeeId, timesheetDate, loggedUserId, "approved");

                            string periodText = string.Empty;
                            if (period != null)
                            {
                                periodText = period.FromDate.ToShortDateString() + " - " + period.ToDate.ToShortDateString();
                            }

                            Task.Factory.StartNew(() => SendApproveEmails(loggedUserId, employeeId, periodText)).ConfigureAwait(false);

                            return Json(new { status = "success", mess = Resource.TimesheetSuccessfullyApproved });
                        }
                        else
                        {
                            return Json(new { status = "invalid", validationSummary = validation.ValidationSummary });
                        }
                    }
                case "revision":
                    {
                        TimesheetGridValidationModel validation = ValidateDistributionTimesheetPeriod(employeeId, timesheetDate, tsAction, loggedUserId);
                        if (string.IsNullOrEmpty(validation.ValidationSummary))
                        {
                            TimesheetPeriodModel periodMOdel = new TimesheetPeriodModel();
                            TimesheetPeriod period = periodMOdel.Update(employeeId, timesheetDate, loggedUserId, "revision");

                            string periodText = string.Empty;
                            if (period != null)
                            {
                                periodText = period.FromDate.ToShortDateString() + " - " + period.ToDate.ToShortDateString();
                            }

                            Task.Factory.StartNew(() => SendRevisionEmails(loggedUserId, employeeId, periodText)).ConfigureAwait(false);

                            return Json(new { status = "success", mess = Resource.TimesheetSuccessfullySentToRevision });
                        }
                        else
                        {
                            return Json(new { status = "invalid", validationSummary = validation.ValidationSummary });
                        }
                    }
                case "validate":
                    {
                        TimesheetGridValidationModel validation = ValidateDistributionTimesheetPeriod(employeeId, timesheetDate, tsAction, loggedUserId);
                        if (string.IsNullOrEmpty(validation.ValidationSummary))
                        {
                            return Json(new { status = "success",mess=Resource.TimesheetValid });
                        }
                        else
                        {
                            return Json(new { status = "invalid", validationSummary = validation.ValidationSummary, mess = Resource.TimesheetNotValid });
                        }
                    }
            }

            return Json(new { status = "" });

        }

        private void SendRevisionEmails(int loggedUserId, int employeeId, string period)
        {
            PERSON emp = EmployeeInfoModel.GetPerson(employeeId);
            PERSON sup = EmployeeInfoModel.GetPerson(loggedUserId);
            List<string> emailList = new List<string>();
            List<string> supNameList = new List<string>();
            if (emp != null)
            {
                EMAILADDRESS empMail = emp.EMAILADDRESSes.FirstOrDefault();
                if (empMail != null && !string.IsNullOrEmpty(empMail.EMAILADDRESSTXT))
                {
                    MailManager.Instance.SendMailMessageTimesheetSentToRevision(empMail.EMAILADDRESSTXT, Resource.TimesheetSentToRevision + " (" + period + ") ", emp.FULLNM, period, sup.FULLNM);
                }
            }
        }

        private void SendApproveEmails(int approverId, int employeeId, string period)
        {
            PERSON emp = EmployeeInfoModel.GetPerson(employeeId);
            PERSON sup = EmployeeInfoModel.GetPerson(approverId);
            List<string> emailList = new List<string>();
            List<string> supNameList = new List<string>();
            if (emp != null)
            {
                EMAILADDRESS empMail = emp.EMAILADDRESSes.FirstOrDefault();
                if (empMail != null && !string.IsNullOrEmpty(empMail.EMAILADDRESSTXT))
                {
                    MailManager.Instance.SendMailMessageTimesheetApproved(empMail.EMAILADDRESSTXT, Resource.TimesheetApproved + " ("+ period+") ", emp.FULLNM,period, sup.FULLNM);
                }
            }
        }

        public void SendSubmitEmails(int employeeId, string period)
        {
            PERSON emp = EmployeeInfoModel.GetPerson(employeeId);
            List<string> emailList = new List<string>();
            List<string> supNameList = new List<string>();
            if (emp != null)
            {
              SupervisorInfoModel.GetSupervisorEmailInfo(employeeId, emailList, supNameList);
                EMAILADDRESS empMail = emp.EMAILADDRESSes.FirstOrDefault();
                if(empMail != null && !string.IsNullOrEmpty(empMail.EMAILADDRESSTXT))
                {
                    MailManager.Instance.SendMailMessageTimsheetWasSubmitted(empMail.EMAILADDRESSTXT, Resource.TimesheetSubmitted+" (" + period+")", emp.FULLNM, period, string.Join(",", supNameList.ToList()));
                }
            }

            if (emailList.Count > 0)
            {
                MailManager.Instance.SendMailMessageTimsheetSubmittedByEmployee(string.Join(",", emailList.ToList()), Resource.TimesheetSubmittedByEmployeeMess + emp.FULLNM + " (" + period+")", string.Join(",", supNameList.ToList()), emp.FULLNM, period);
            }
        }

        public JsonResult PeriodInfo(int employeeId, DateTime timesheetDate)
        {
            string status = string.Empty;
            string statusCss = string.Empty;

            status = Resource.NotCreated;
            statusCss = "notcreated";

            TimesheetPeriod period = new TimesheetPeriodModel().GetTimesheetPeriod(employeeId, timesheetDate);
            if (period != null)
            {
                statusCss = period.Status;
                switch (period.Status)
                {
                    case "pendingsubmission":
                        {
                            status = Resource.PendingToSubmit;
                            break;
                        }
                    case "submitted":
                        {
                            status = Resource.Submitted;
                            break;
                        }
                    case "approved":
                        {
                            status = Resource.Approved;
                            break;
                        }
                    case "revision":
                        {
                            status = Resource.SentToRevision;
                            break;
                        }
                    case "senttosap":
                        {
                            status = Resource.SentToSap;
                            break;
                        }
                }
            }
            return Json(new { periodStatus = status, css = statusCss });
        }

        public JsonResult GetCalendarInfo(int employeeId, DateTime fromDate, DateTime toDate)
        {
            string notValid = string.Empty;
            string pendingToSubmit = string.Empty;
            string approved = string.Empty;
            string submitted = string.Empty;
            string senttosap = string.Empty;
            TimesheetPeriod period = null;

            if (toDate.Subtract(fromDate).Days < 60)
            {
                WfcTotalModel wfc = new WfcTotalModel();
                List<WfcTotalModel> list = wfc.GetWfcTotal(employeeId, fromDate, toDate);

                var query = (from total in list
                             group total by new { total.TimesheetDate } into grp
                             select new { Date = grp.Key.TimesheetDate, Total = grp.Sum(c => c.TimesheeTotal) }).ToList();

                List<TimesheetPeriod> periods = new TimesheetPeriodModel().GetTimesheetPeriod(employeeId, fromDate, toDate);
                decimal dayTotal = 0;
                foreach (var item in query)
                {
                    period = periods.Where(p => p.FromDate <= item.Date && p.ToDate >= item.Date).FirstOrDefault();
                    if (period != null)
                    {
                        switch (period.Status)
                        {
                            case "approved":
                                {
                                    approved = approved + ".k-link[data-value='" + item.Date.Year.ToString() + "/" + (item.Date.Month - 1).ToString() + "/" + item.Date.Day.ToString() + "'],";
                                    break;
                                }
                            case "submitted":
                                {
                                    submitted = submitted + ".k-link[data-value='" + item.Date.Year.ToString() + "/" + (item.Date.Month - 1).ToString() + "/" + item.Date.Day.ToString() + "'],";
                                    break;
                                }
                            case "senttosap":
                                {
                                    senttosap = senttosap + ".k-link[data-value='" + item.Date.Year.ToString() + "/" + (item.Date.Month - 1).ToString() + "/" + item.Date.Day.ToString() + "'],";
                                    break;
                                }
                            case "revision":
                            case "pendingsubmission":
                                {
                                    switch (item.Date.DayOfWeek)
                                    {
                                        case DayOfWeek.Sunday:
                                            {
                                                dayTotal = period.TimesheetDistributions.Sum(d => d.SundayRegular + d.SundayOvertime);
                                                if (dayTotal < item.Total)
                                                {
                                                    notValid = notValid + ".k-link[data-value='" + item.Date.Year.ToString() + "/" + (item.Date.Month - 1).ToString() + "/" + item.Date.Day.ToString() + "'],";
                                                }
                                                else
                                                {
                                                    pendingToSubmit = pendingToSubmit + ".k-link[data-value='" + item.Date.Year.ToString() + "/" + (item.Date.Month - 1).ToString() + "/" + item.Date.Day.ToString() + "'],";
                                                }

                                                break;
                                            }
                                        case DayOfWeek.Monday:
                                            {
                                                dayTotal = period.TimesheetDistributions.Sum(d => d.MondayRegular + d.MondayOvertime);
                                                if (dayTotal < item.Total)
                                                {
                                                    notValid = notValid + ".k-link[data-value='" + item.Date.Year.ToString() + "/" + (item.Date.Month - 1).ToString() + "/" + item.Date.Day.ToString() + "'],";
                                                }
                                                else
                                                {
                                                    pendingToSubmit = pendingToSubmit + ".k-link[data-value='" + item.Date.Year.ToString() + "/" + (item.Date.Month - 1).ToString() + "/" + item.Date.Day.ToString() + "'],";
                                                }

                                                break;
                                            }
                                        case DayOfWeek.Tuesday:
                                            {
                                                dayTotal = period.TimesheetDistributions.Sum(d => d.TuesdayRegular + d.TuesdayOvertime);
                                                if (dayTotal < item.Total)
                                                {
                                                    notValid = notValid + ".k-link[data-value='" + item.Date.Year.ToString() + "/" + (item.Date.Month - 1).ToString() + "/" + item.Date.Day.ToString() + "'],";
                                                }
                                                else
                                                {
                                                    pendingToSubmit = pendingToSubmit + ".k-link[data-value='" + item.Date.Year.ToString() + "/" + (item.Date.Month - 1).ToString() + "/" + item.Date.Day.ToString() + "'],";
                                                }

                                                break;
                                            }
                                        case DayOfWeek.Wednesday:
                                            {
                                                dayTotal = period.TimesheetDistributions.Sum(d => d.WednesdayRegular + d.WednesdayOvertime);
                                                if (dayTotal < item.Total)
                                                {
                                                    notValid = notValid + ".k-link[data-value='" + item.Date.Year.ToString() + "/" + (item.Date.Month - 1).ToString() + "/" + item.Date.Day.ToString() + "'],";
                                                }
                                                else
                                                {
                                                    pendingToSubmit = pendingToSubmit + ".k-link[data-value='" + item.Date.Year.ToString() + "/" + (item.Date.Month - 1).ToString() + "/" + item.Date.Day.ToString() + "'],";
                                                }

                                                break;
                                            }
                                        case DayOfWeek.Thursday:
                                            {
                                                dayTotal = period.TimesheetDistributions.Sum(d => d.ThursdayRegular + d.ThursdayOvertime);
                                                if (dayTotal < item.Total)
                                                {
                                                    notValid = notValid + ".k-link[data-value='" + item.Date.Year.ToString() + "/" + (item.Date.Month - 1).ToString() + "/" + item.Date.Day.ToString() + "'],";
                                                }
                                                else
                                                {
                                                    pendingToSubmit = pendingToSubmit + ".k-link[data-value='" + item.Date.Year.ToString() + "/" + (item.Date.Month - 1).ToString() + "/" + item.Date.Day.ToString() + "'],";
                                                }

                                                break;
                                            }
                                        case DayOfWeek.Friday:
                                            {
                                                dayTotal = period.TimesheetDistributions.Sum(d => d.FridayRegular + d.FridayOvertime);
                                                if (dayTotal < item.Total)
                                                {
                                                    notValid = notValid + ".k-link[data-value='" + item.Date.Year.ToString() + "/" + (item.Date.Month - 1).ToString() + "/" + item.Date.Day.ToString() + "'],";
                                                }
                                                else
                                                {
                                                    pendingToSubmit = pendingToSubmit + ".k-link[data-value='" + item.Date.Year.ToString() + "/" + (item.Date.Month - 1).ToString() + "/" + item.Date.Day.ToString() + "'],";
                                                }

                                                break;
                                            }
                                        case DayOfWeek.Saturday:
                                            {
                                                dayTotal = period.TimesheetDistributions.Sum(d => d.SaturdayRegular + d.SaturdayOvertime);
                                                if (dayTotal < item.Total)
                                                {
                                                    notValid = notValid + ".k-link[data-value='" + item.Date.Year.ToString() + "/" + (item.Date.Month - 1).ToString() + "/" + item.Date.Day.ToString() + "'],";
                                                }
                                                else
                                                {
                                                    pendingToSubmit = pendingToSubmit + ".k-link[data-value='" + item.Date.Year.ToString() + "/" + (item.Date.Month - 1).ToString() + "/" + item.Date.Day.ToString() + "'],";
                                                }

                                                break;
                                            }
                                    }

                                    break;
                                }
                        }
                    }
                    else
                    {
                        notValid = notValid + ".k-link[data-value='" + item.Date.Year.ToString() + "/" + (item.Date.Month - 1).ToString() + "/" + item.Date.Day.ToString() + "'],";
                    }
                }
            }


            return Json(new
            {
                notValid = notValid.TrimEnd(','),
                pendingToSubmit = pendingToSubmit.TrimEnd(','),
                submitted = submitted.TrimEnd(','),
                approved = approved.TrimEnd(','),
                senttosap = senttosap.TrimEnd(','),
            });
        }
    }
}