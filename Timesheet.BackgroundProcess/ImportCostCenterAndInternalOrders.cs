﻿using CsvHelper;
using Models;
using NLog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Timesheet.BackgroundProcess
{
    class ImportCostCenterAndInternalOrders
    {
        public static Logger logger = LogManager.GetCurrentClassLogger();
        SSSEntities sssEntities = new SSSEntities();

        string filesPath = ConfigurationManager.AppSettings["SapImportFilesFolder"];
        public static void Main(string[] args)
        {  
            ImportCostCenterAndInternalOrders program = new ImportCostCenterAndInternalOrders();
            program.Execute();          
        }

        public void Execute()
        {
            sssEntities.AppSections.FirstOrDefault();
            IList<CostCenter> costCenteres = GetCostCentersFromFile();
            foreach (CostCenter c in costCenteres)
            {
                bool exists = sssEntities.CostCenters.Any(x => x.Code == c.Code);
                if (exists)
                    continue;
                sssEntities.CostCenters.Add(c);
                sssEntities.SaveChanges();
            }

            foreach (InternalOrder i in GetInternalOrdersFromFile())
            {
                bool exists = sssEntities.InternalOrders.Any(x => x.Code == i.Code);
                if (exists)
                    continue;
                sssEntities.InternalOrders.Add(i);
                sssEntities.SaveChanges();
            }

            sssEntities.SaveChanges();
        }

        protected IList<CostCenter> GetCostCentersFromFile()
        {
           
            IList<CostCenter> costCenters = new List<CostCenter>();
            using (var reader = new StreamReader(Path.Combine(filesPath, "CostCenters.txt")))
            using (var csv = new CsvReader(reader))
            {
                csv.Configuration.HasHeaderRecord = false;
                csv.Configuration.Delimiter = "\t";
                csv.Configuration.TypeConverterOptionsCache.GetOptions<DateTime>().Formats = new[] { "M/dd/yyyy" };


                while (csv.Read())
                {
                    var record = new CostCenter
                    {
                        Code = csv.GetField<string>(0),
                        ValidDate = csv.GetField<DateTime>(1),
                        Name = csv.GetField<string>(3)
                    };
                    record.CompanyCode = record.Code.Substring(0, 4);
                    costCenters.Add(record);
                }
            }

            return costCenters;
        }

        protected IEnumerable<InternalOrder> GetInternalOrdersFromFile()
        {
            IList<InternalOrder> internalOrders = new List<InternalOrder>();
            using (var reader = new StreamReader(Path.Combine(filesPath, "InternalOrders.txt")))
            using (var csv = new CsvReader(reader))
            {
                csv.Configuration.HasHeaderRecord = false;
                csv.Configuration.Delimiter = "\t";
                while (csv.Read())
                {
                    var record = new InternalOrder
                    {
                        Code = csv.GetField<string>(0),
                        Name = csv.GetField<string>(1),
                        CompanyCode = csv.GetField<string>(2)
                    };
                    internalOrders.Add(record);
                }
            }

            return internalOrders;
        }
    }
}
